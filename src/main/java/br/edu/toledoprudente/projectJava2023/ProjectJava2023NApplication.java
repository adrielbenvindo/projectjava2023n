package br.edu.toledoprudente.projectJava2023;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectJava2023NApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectJava2023NApplication.class, args);
	}

}
