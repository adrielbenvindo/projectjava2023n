package br.edu.toledoprudente.projectJava2023.controller;

import java.util.List;
import java.util.Set;
import java.util.Optional;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.TamanhoDAO;
import br.edu.toledoprudente.projectJava2023.dao.ArmazemDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Tamanho;
import br.edu.toledoprudente.projectJava2023.pojo.Armazem;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

@Controller
@RequestMapping("/armazem")
public class ArmazemController {
	
	
	@Autowired
	private ArmazemDAO dao;
	
	@Autowired
	private TamanhoDAO dao_tamanho;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("armazem", new Armazem());
		return "/armazem/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/armazem/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		try{
			model.addAttribute("armazem", (dao.findById(id)).get());
		}catch(NoSuchElementException exception){
			model.addAttribute("armazem", new Armazem());
			model.addAttribute("mensagem", "Armazem nao encontrado");
			model.addAttribute("retorno", false);
		}
		return "/armazem/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			Optional<Armazem> data = dao.findById(id);
			if(data.isPresent()){
				dao.delete(id);
				model.addAttribute("mensagem", "Exclusão efetuada");
				model.addAttribute("retorno",true);
			}else{
				model.addAttribute("mensagem", "Armazem nao encontrado!");
				model.addAttribute("retorno",false);
			}
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/armazem/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("cliente") Armazem armazem, ModelMap model) {
		try {
		    Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Armazem>> constraintViolations = validator.validate( armazem );
            String errors = "";
            for (ConstraintViolation<Armazem> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("armazem", armazem);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/armazem/index";
            }else{
				if(armazem.getId()==null)
					dao.save(Optional.of(armazem));
				else
					dao.update(Optional.of(armazem));
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno",false);
		}
		
		return "/cliente/index";
	}
	/*método usado pra retornar dados para select html*/
	@ModelAttribute(name = "list_tamanho")
	public List<Tamanho> listTamanho(){
		return dao_tamanho.findAll();
		
	}
	
}
