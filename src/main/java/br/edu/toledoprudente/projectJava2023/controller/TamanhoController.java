package br.edu.toledoprudente.projectJava2023.controller;

import java.util.Set;
import java.util.Optional;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.TamanhoDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Tamanho;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

@Controller
@RequestMapping("/tamanho")
public class TamanhoController {
	
	
	@Autowired
	private TamanhoDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("tamanho", new Tamanho());
		return "/tamanho/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/tamanho/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		try{
			model.addAttribute("tamanho", (dao.findById(id)).get());
		}catch(NoSuchElementException exception){
			model.addAttribute("tamanho", new Tamanho());
			model.addAttribute("mensagem", "Tamanho nao encontrado");
			model.addAttribute("retorno", false);
		}
		
		return "/tamanho/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id") int id, ModelMap model) {
		try {
			Optional<Tamanho> data = dao.findById(id);
			if(data.isPresent()){
				dao.delete(id);
				model.addAttribute("mensagem","Exclusão efetuada");
				model.addAttribute("retorno",true);
			}else{
				model.addAttribute("mensagem", "Tamanho nao encontrado!");
				model.addAttribute("retorno",false);
			}
		}
		catch (Exception e) {
			model.addAttribute("mensagem","Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/tamanho/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("tamanho") Tamanho tamanho, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Tamanho>> constraintViolations = validator.validate( tamanho );
            String errors = "";
            for (ConstraintViolation<Tamanho> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("tamanho", tamanho);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/tamanho/index";
            }else{
				if(tamanho.getId()==null)
					dao.save(Optional.of(tamanho));
				else
					dao.update(Optional.of(tamanho));
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/tamanho/index";
	}
	
}
