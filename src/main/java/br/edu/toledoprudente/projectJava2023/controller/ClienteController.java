package br.edu.toledoprudente.projectJava2023.controller;

import java.util.Set;
import java.util.HashSet;
import java.util.Optional;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.ClienteDAO;
import br.edu.toledoprudente.projectJava2023.dao.UsuarioDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Cliente;
import br.edu.toledoprudente.projectJava2023.pojo.Usuario;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

import br.edu.toledoprudente.projectJava2023.pojo.AppAuthority;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Controller
@RequestMapping("/Cliente")
public class ClienteController {
	
	
	@Autowired
	private ClienteDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		Cliente cliente = new Cliente();
		cliente.setUsuarioSafe(Optional.of(new Usuario()));
		model.addAttribute("cliente", cliente);
		return "/cliente/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/cliente/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		try{
			model.addAttribute("cliente", (dao.findById(id)).get());
		}catch(NoSuchElementException exception){
			Cliente cliente = new Cliente();
			cliente.setUsuarioSafe(Optional.of(new Usuario()));
			model.addAttribute("cliente", cliente);
			model.addAttribute("mensagem", "Cliente nao encontrado");
			model.addAttribute("retorno", false);
		}
		
		return "/cliente/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id") int id, ModelMap model) {
		try {
			Optional<Cliente> data = dao.findById(id);
			if(data.isPresent()){
				dao.delete(id);
				model.addAttribute("mensagem", "Exclusão efetuada");
				model.addAttribute("retorno",true);
			}else{
				model.addAttribute("mensagem", "Cliente nao encontrado!");
				model.addAttribute("retorno",false);
			}
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/cliente/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("cliente") Cliente cliente, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Cliente>> constraintViolations = validator.validate( cliente );
            String errors = "";
            for (ConstraintViolation<Cliente> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("cliente", cliente);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/cliente/index";
            }else{
				Optional<Usuario> usuario = cliente.getUsuarioSafe();
				String password = "{bcrypt}" + (new BCryptPasswordEncoder()).encode(((usuario.get()).getPasswordSafe()).get());
				(usuario.get()).setPasswordSafe(Optional.of(password));
				(usuario.get()).setEnabledSafe(Optional.of(true));
				
				//setar a autorização
				Set<AppAuthority> appAuthorities = new HashSet<AppAuthority>();
				AppAuthority app = new AppAuthority();
				app.setAuthoritySafe(Optional.of("USER"));
				app.setUsernameSafe((usuario.get()).getUsernameSafe());
			    appAuthorities.add(app);
			    (usuario.get()).setAppAuthoritiesSafe(Optional.of(appAuthorities));
				
				if(cliente.getId()==null)
					dao.save(Optional.of(cliente));
				else
					dao.update(Optional.of(cliente));
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/cliente/index";
	}
	
}
