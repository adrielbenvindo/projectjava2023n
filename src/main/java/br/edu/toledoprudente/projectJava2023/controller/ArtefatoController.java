package br.edu.toledoprudente.projectJava2023.controller;

import java.util.Set;
import java.util.Optional;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.ArtefatoDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Artefato;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

@Controller
@RequestMapping("/artefato")
public class ArtefatoController {
	
	
	@Autowired
	private ArtefatoDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("artefato", new Artefato());
		return "/artefato/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/artefato/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		try{
			model.addAttribute("artefato", (dao.findById(id)).get());
		}catch(NoSuchElementException exception){
			model.addAttribute("artefato", new Artefato());
			model.addAttribute("mensagem", "Artefato nao encontrado");
			model.addAttribute("retorno", false);
		}
		
		return "/artefato/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			Optional<Artefato> data = dao.findById(id);
			if(data.isPresent()){
				dao.delete(id);
				model.addAttribute("mensagem", "Exclusão efetuada");
				model.addAttribute("retorno",true);
			}else{
				model.addAttribute("mensagem", "Artefato nao encontrado!");
				model.addAttribute("retorno",false);
			}
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/artefato/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("artefato") Artefato artefato, ModelMap model) {
		try {
		    Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Artefato>> constraintViolations = validator.validate( artefato );
            String errors = "";
            for (ConstraintViolation<Artefato> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("artefato",artefato);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/artefato/index";
            }else{
				if(artefato.getId()==null)
					dao.save(Optional.of(artefato));
				else
					dao.update(Optional.of(artefato));
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/artefato/index";
	}
	
}
