package br.edu.toledoprudente.projectJava2023.controller;

import java.util.Set;
import java.util.HashSet;
import java.util.Optional;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.FuncionarioDAO;
import br.edu.toledoprudente.projectJava2023.dao.UsuarioDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Funcionario;
import br.edu.toledoprudente.projectJava2023.pojo.Usuario;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

import br.edu.toledoprudente.projectJava2023.pojo.AppAuthority;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Controller
@RequestMapping("/funcionario")
public class FuncionarioController {
	
	
	@Autowired
	private FuncionarioDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		Funcionario funcionario = new Funcionario();
		funcionario.setUsuario(new Usuario());
		model.addAttribute("funcionario", funcionario);
		return "/funcionario/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/funcionario/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		try{
			model.addAttribute("funcionario", (dao.findById(id)).get());
		}catch(NoSuchElementException exception){
			Funcionario funcionario = new Funcionario();
			funcionario.setUsuarioSafe(Optional.of(new Usuario()));
			model.addAttribute("funcionario", funcionario);
			model.addAttribute("mensagem", "Funcionario nao encontrado");
			model.addAttribute("retorno", false);
		}
		
		return "/funcionario/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			Optional<Funcionario> data = dao.findById(id);
			if(data.isPresent()){
				dao.delete(id);
				model.addAttribute("mensagem", "Exclusão efetuada");
				model.addAttribute("retorno",true);
			}else{
				model.addAttribute("mensagem", "Funcionario nao encontrado!");
				model.addAttribute("retorno",false);
			}
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("lista", dao.findAll());
		return "/funcionario/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("funcionario") Funcionario funcionario, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Funcionario>> constraintViolations = validator.validate( funcionario );
            String errors = "";
            for (ConstraintViolation<Funcionario> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("funcionario",funcionario);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/funcionario/index";
            }else{
				Optional<Usuario> usuario = funcionario.getUsuarioSafe();
				String password = "{bcrypt}" + (new BCryptPasswordEncoder()).encode(((usuario.get()).getPasswordSafe()).get());
				(usuario.get()).setPasswordSafe(Optional.of(password));
				(usuario.get()).setEnabledSafe(Optional.of(true));
			
				//setar a autorização
				Set<AppAuthority> appAuthorities = new HashSet<AppAuthority>();
				AppAuthority app = new AppAuthority();
				app.setAuthoritySafe(Optional.of("USER"));
				app.setUsernameSafe((usuario.get()).getUsernameSafe());
			    appAuthorities.add(app);
			    (usuario.get()).setAppAuthoritiesSafe(Optional.of(appAuthorities));
				
				if(funcionario.getId()==null)
					dao.save(Optional.of(funcionario));
				else
					dao.update(Optional.of(funcionario));
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/funcionario/index";
	}
	
}
