package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import br.edu.toledoprudente.projectJava2023.dao.ArtefatoDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Artefato;

@Component
public class StringToArtefatoConverter implements Converter <String, Artefato> {

	@Autowired
	private ArtefatoDAO dao;

	@Override
	
	public Artefato convert(String idText) {
		// TODO Auto-generated method stub
		if(idText.isEmpty())
			return null;

		return (dao.findById(Integer.parseInt( idText))).get();
	}
}
