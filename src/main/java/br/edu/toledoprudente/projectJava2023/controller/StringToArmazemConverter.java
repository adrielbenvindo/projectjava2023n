package br.edu.toledoprudente.projectJava2023.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import br.edu.toledoprudente.projectJava2023.dao.ArmazemDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Armazem;

@Component
public class StringToArmazemConverter implements Converter <String, Armazem> {

	@Autowired
	private ArmazemDAO dao;

	@Override
	public Armazem convert(String idText) {
		// TODO Auto-generated method stub
		if(idText.isEmpty())
			return null;

		return (dao.findById(Integer.parseInt( idText))).get();
	}
}
