package br.edu.toledoprudente.projectJava2023.controller;

import java.util.Set;
import java.util.Optional;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.CategoriaDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Categoria;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

@Controller
@RequestMapping("/categoria")
public class CategoriaController {
	
	
	@Autowired
	private CategoriaDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("categoria", 
				new Categoria());
		return "/categoria/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/categoria/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		try{
			model.addAttribute("categoria",(dao.findById(id)).get());
		}catch(NoSuchElementException exception){
			model.addAttribute("categoria", new Categoria());
			model.addAttribute("mensagem", "Categoria nao encontrada");
			model.addAttribute("retorno", false);
		}
		
		return "/categoria/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			Optional<Categoria> data = dao.findById(id);
			if(data.isPresent()){
				dao.delete(id);
				model.addAttribute("mensagem", "Exclusão efetuada");
				model.addAttribute("retorno",true);
			}else{
				model.addAttribute("mensagem", "Categoria nao encontrada!");
				model.addAttribute("retorno",false);
			}
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/categoria/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(
			@ModelAttribute("categoria") 
			Categoria categoria, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Categoria>> constraintViolations = validator.validate( categoria );
            String errors = "";
            for (ConstraintViolation<Categoria> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("categoria", categoria);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/categoria/index";
            }else{
				if(categoria.getId()==null)
					dao.save(Optional.of(categoria));
				else
					dao.update(Optional.of(categoria));
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/categoria/index";
	}
	
}
