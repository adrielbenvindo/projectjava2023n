package br.edu.toledoprudente.projectJava2023.controller;

import java.util.List;
import java.util.Set;
import java.util.Optional;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.UnidadeDAO;
import br.edu.toledoprudente.projectJava2023.dao.ClienteDAO;
import br.edu.toledoprudente.projectJava2023.dao.VendaDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Unidade;
import br.edu.toledoprudente.projectJava2023.pojo.Cliente;
import br.edu.toledoprudente.projectJava2023.pojo.Venda;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

@Controller
@RequestMapping("/venda")
public class VendaController {
	
	
	@Autowired
	private VendaDAO dao;
	
	@Autowired
	private UnidadeDAO dao_unidade;
	
	@Autowired
	private ClienteDAO dao_cliente;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("venda", new Venda());
		return "/venda/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/venda/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		try{
			model.addAttribute("venda", (dao.findById(id)).get());
		}catch(NoSuchElementException exception){
			model.addAttribute("venda", new Venda());
			model.addAttribute("mensagem", "Venda nao encontrada");
			model.addAttribute("retorno", false);
		}
		
		return "/venda/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			Optional<Venda> data = dao.findById(id);
			if(data.isPresent()){
				dao.delete(id);
				model.addAttribute("mensagem", "Exclusão efetuada");
				model.addAttribute("retorno",true);
			}else{
				model.addAttribute("mensagem", "Venda nao encontrada!");
				model.addAttribute("retorno",false);
			}
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/venda/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("venda") Venda venda, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Venda>> constraintViolations = validator.validate( venda );
            String errors = "";
            for (ConstraintViolation<Venda> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("venda",venda);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/venda/index";
            }else{
				if(venda.getId()==null)
					dao.save(Optional.of(venda));
				else
					dao.update(Optional.of(venda));
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/venda/index";
	}
	/*método usado pra retornar dados para select html*/
	@ModelAttribute(name = "list_unidade")
	public List<Unidade> listUnidade(){
		return dao_unidade.findAll();
		
	}
	
	@ModelAttribute(name = "list_cliente")
	public List<Cliente> listCliente(){
		return dao_cliente.findAll();
		
	}
	
}
