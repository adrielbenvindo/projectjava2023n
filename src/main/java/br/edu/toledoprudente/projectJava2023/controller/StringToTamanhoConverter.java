package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import br.edu.toledoprudente.projectJava2023.dao.TamanhoDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Tamanho;

@Component
public class StringToTamanhoConverter implements Converter <String, Tamanho> {

	@Autowired
	private TamanhoDAO dao;

	@Override
	public Tamanho convert(String idText) {
		// TODO Auto-generated method stub
		if(idText.isEmpty())
			return null;

		return (dao.findById(Integer.parseInt( idText))).get();
	}
}
