package br.edu.toledoprudente.projectJava2023.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Optional;
import java.util.NoSuchElementException;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import br.edu.toledoprudente.projectJava2023.dao.ArmazemDAO;
import br.edu.toledoprudente.projectJava2023.dao.CategoriaDAO;
import br.edu.toledoprudente.projectJava2023.dao.ArtefatoDAO;
import br.edu.toledoprudente.projectJava2023.dao.UnidadeDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Armazem;
import br.edu.toledoprudente.projectJava2023.pojo.Categoria;
import br.edu.toledoprudente.projectJava2023.pojo.Artefato;
import br.edu.toledoprudente.projectJava2023.pojo.Unidade;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;

@Controller
@RequestMapping("/unidade")
public class UnidadeController {
	
	
	@Autowired
	private UnidadeDAO dao;
	
	@Autowired
	private ArmazemDAO dao_armazem;
	
	@Autowired
	private CategoriaDAO dao_categoria;
	
	@Autowired
	private ArtefatoDAO dao_artefato;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("unidade", new Unidade());
		return "/unidade/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/unidade/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		try{
			model.addAttribute("unidade", (dao.findById(id)).get());
		}catch(NoSuchElementException exception){
			model.addAttribute("unidade", new Unidade());
			model.addAttribute("mensagem", "Unidade nao encontrada");
			model.addAttribute("retorno", false);
		}
		
		return "/unidade/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			Optional<Unidade> data = dao.findById(id);
			if(data.isPresent()){
				dao.delete(id);
				model.addAttribute("mensagem", "Exclusão efetuada");
				model.addAttribute("retorno",true);
			}else{
				model.addAttribute("mensagem", "Unidade nao encontrado!");
				model.addAttribute("retorno",false);
			}
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/unidade/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("unidade") Unidade unidade, ModelMap model, @RequestParam("file") MultipartFile file) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Unidade>> constraintViolations = validator.validate( unidade );
            String errors = "";
            for (ConstraintViolation<Unidade> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("unidade", unidade);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/unidade/index";
            }else{
            	Random random = new Random();
				String fileName = random.nextInt() + file.getOriginalFilename();
			    unidade.setImageSafe(Optional.of(fileName));
				if(unidade.getId()==null)
					dao.save(Optional.of(unidade));
				else
					dao.update(Optional.of(unidade));
				try {        
					byte[] bytes = file.getBytes();
            		Path path = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\image\\" + fileName);
            		Files.write(path, bytes);
        		}catch (IOException e){
            		e.printStackTrace();        
                }
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/unidade/index";
	}
	/*método usado pra retornar dados para select html*/
	@ModelAttribute(name = "list_armazem")
	public List<Armazem> listArmazem(){
		return dao_armazem.findAll();
		
	}
	
	@ModelAttribute(name = "list_artefato")
	public List<Artefato> listArtefato(){
		return dao_artefato.listAllRelated();
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/getimage/{name}", method = RequestMethod.GET)
	public HttpEntity<byte[]> download(@PathVariable(value = "name") String name) throws IOException {
		byte[] file = Files.readAllBytes( Paths.get(System.getProperty("user.dir") +"\\src\\main\\resources\\static\\image\\" + name));
		HttpHeaders httpHeaders = new HttpHeaders();
		switch (name.substring(name.lastIndexOf(".") + 1).toUpperCase()) {
    		case "JPG":
    			httpHeaders.setContentType(MediaType.IMAGE_JPEG);
    			break;
			case "GIF":
				httpHeaders.setContentType(MediaType.IMAGE_GIF); 
				break;
			case "PNG":
				httpHeaders.setContentType(MediaType.IMAGE_PNG); 
				break;
			default:
				break;
		}        
		httpHeaders.setContentLength(file.length);
		HttpEntity<byte[]> entity = new HttpEntity<byte[]>( file, httpHeaders);
		return entity;
	}
}
