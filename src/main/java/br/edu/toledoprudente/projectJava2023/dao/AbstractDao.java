package br.edu.toledoprudente.projectJava2023.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.NoSuchElementException;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;



public abstract class AbstractDao<T, PK extends Serializable> {

	@SuppressWarnings("unchecked")
	private final Class<T> entityClass = 
			(Class<T>) ( (ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	
	@PersistenceContext
	private EntityManager entityManager;

	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Transactional
	public void save(Optional<T> entity) { 
		if(entity.isPresent()){
			entityManager.persist(entity.get());
		}
	}
	
	@Transactional
	public void update(Optional<T> entity) {
		if(entity.isPresent()){
			entityManager.merge(entity.get());
		}
	}
	
	@Transactional
	public void delete(PK id) {
		entityManager.remove(entityManager.getReference(entityClass, id));	
	}
	
	public Optional<T> findById(PK id) {
		
		T data = entityManager.find(entityClass, id);
		if(data == null){
			return Optional.empty();
		}
		return Optional.of(data);
	}
	
	public List<T> findAll() {
		
		return entityManager
				.createQuery("from " + entityClass.getSimpleName(), entityClass)
				.getResultList();
	}	
	
	protected List<T> createQuery(Optional<String> jpql, Object... params) {
		try{
			if (!jpql.isPresent())
				return new ArrayList<T>();
			TypedQuery<T> query = entityManager.createQuery(jpql.get(), entityClass);
			for (int i = 0; i < params.length; i++) {
		    	query.setParameter(i+1, params[i]);
			}
			return query.getResultList();
		}catch(NoSuchElementException exception){
			return new ArrayList<T>();
		}
	}
}
