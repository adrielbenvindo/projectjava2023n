package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.edu.toledoprudente.projectJava2023.pojo.Categoria;
import br.edu.toledoprudente.projectJava2023.pojo.Usuario;

import java.util.List;
import java.util.Optional;

@Repository
public class UsuarioDAO extends AbstractDao<Usuario,Integer> {
	public Usuario getUsuarioLogged() {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username;
		if (principal instanceof UserDetails) {
			username = ((UserDetails)principal).getUsername();
		} else {
			username = principal.toString();
		}
		return findByUserName(username);
	}
	
	public Optional<Usuario> getUsuarioLoggedSafe(){
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username;
		if(principal instanceof UserDetails){
			username = ((UserDetails)principal).getUsername();
		}else{
			username = principal.toString();
		}
		if (username == null)
				return Optional.empty();
		return findByUserNameSafe(Optional.of(username));
	}
	
	public Usuario findByUserName(String username) {
		List<Usuario> list = this.createQuery(Optional.of("select u from Usuario u where u.username like ?1"), username) ;
		return list.isEmpty() ? null : list.get(0);
	}
	
	public Optional<Usuario> findByUserNameSafe(Optional<String> username){
		if (!username.isPresent())
			return Optional.empty();
		List<Usuario> list = this.createQuery(Optional.of("select u from Usuario u where u.username like ?1"), username.get());
		return list.isEmpty() ? Optional.empty() : Optional.of(list.get(0));
	}
	
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario user = findByUserName(username);
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), user.getAppAuthorities());
	}
	
	public Optional<UserDetails> loadUserByUsernameSafe(Optional<String> username) throws UsernameNotFoundException{
		Optional<Usuario> user = findByUserNameSafe(username);
		if (!user.isPresent())
			throw new UsernameNotFoundException("Username not found");
		UserDetails data =  new org.springframework.security.core.userdetails.User(((user.get()).getUsernameSafe().get()), ((user.get()).getPasswordSafe().get()), ((user.get()).getAppAuthoritiesSafe().get()));
		if (data == null)
			return Optional.empty();
		return Optional.of(data);
	}
		
}



