package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Cliente;

@Repository
public class ClienteDAO 
extends AbstractDao<Cliente, Integer> {

}
