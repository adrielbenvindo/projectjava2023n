package br.edu.toledoprudente.projectJava2023.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Artefato;

@Repository
public class ArtefatoDAO 
extends AbstractDao<Artefato, Integer> {
	public List<Artefato> listAllRelated(){
		return createQuery(Optional.of("select a from Artefato a join fetch a.categoria"));
	}
}
