package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Categoria;

@Repository
public class CategoriaDAO 
extends AbstractDao<Categoria, Integer> {

}
