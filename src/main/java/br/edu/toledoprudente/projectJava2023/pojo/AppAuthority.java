package br.edu.toledoprudente.projectJava2023.pojo;

import java.io.Serializable;
import java.util.Optional;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

@Entity
@Table(name = "authorities")
public class AppAuthority extends AbstractEntity<Integer> implements GrantedAuthority, Serializable {
//~ Instance fields ================================================================================================
  @Column(name = "username", nullable = false)
  private String username;
  @Column(name = "authority", nullable = false)
  private String authority;
  // Here comes the buggy attribute. It is supposed to repesent the
  // association username<->username, but I just don't know how to
  // implement it 
  @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @JoinColumn(name = "appuser_fk")
  private Usuario appUser;

  //~ Constructors ===================================================================================================
  public AppAuthority(String username, String authority) {
    Assert.hasText(authority, "A granted authority textual representation is required");
    this.username = username;
    this.authority = authority;
  }

  public AppAuthority() {
  
  }
  
  @Override
  public String getAuthority() {
    //TODO Auto-generated method stub
    return null;
  }
  
  public String getUsername() {
    return username;
  }
  
  public void setUsername(String username) {
    this.username = username;
  }
  
  public Optional<String> getUsernameSafe(){
      if(username == null){
			return Optional.empty();
      }
      return Optional.of(username);
  }

  public void setUsernameSafe(Optional<String> username){
      if(username.isPresent()){
		this.username = username.get();
      }else{
		this.username = null;
      }
  }
  
  public Usuario getAppUser() {
    return appUser;
  }
  
  public void setAppUser(Usuario appUser) {
    this.appUser = appUser;
  }
  
  public Optional<Usuario> getAppUserSafe(){
      if(appUser == null){
			return Optional.empty();
      }
      return Optional.of(appUser);
  }

  public void setAppUserSafe(Optional<Usuario> appUser){
      if(appUser.isPresent()){
		this.appUser = appUser.get();
      }else{
		this.appUser = null;
      }
  }
  
  public void setAuthority(String authority) {
    this.authority = authority;
  }
  
  public Optional<String> getAuthoritySafe(){
      if(authority == null){
			return Optional.empty();
      }
      return Optional.of(authority);
  }

  public void setAuthoritySafe(Optional<String> authority){
      if(authority.isPresent()){
		this.authority = authority.get();
      }else{
		this.authority = null;
      }
  }
  // Getters 'n setters 'n other stuff
}
