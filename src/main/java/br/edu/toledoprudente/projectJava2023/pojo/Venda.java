package br.edu.toledoprudente.projectJava2023.pojo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@Table(name = "venda")
public class Venda extends AbstractEntity<Integer> {
	
	@NotBlank(message = "Informe o nome")
	@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
	@Column(length = 150, nullable = false)
	private String nome;

	@PositiveOrZero(message = "Informe uma quantidade total igual ou maior que 0")
	@Column(nullable = false)
	private Integer quantidade;
	
	@PositiveOrZero(message = "Informe um valor total igual ou maior que 0")
	@Column(name="valor_total", nullable = true, columnDefinition = "DECIMAL(10,2) DEFAULT 0.00")
	private BigDecimal valorTotal;
	
	@PastOrPresent(message = "Selecione uma data de venda")
	@Column(name="data_venda", nullable = false, columnDefinition = "DATE")
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate dataVenda;
	
	@NotNull(message = "Selecione o cliente")
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;
	
	@NotNull(message = "Selecione a unidade")
	@ManyToOne
	@JoinColumn(name="unidade_id")
	private Unidade unidade;
	
	

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Optional<Cliente> getClienteSafe(){
		if(cliente == null){
			return Optional.empty();
		}
		return Optional.of(cliente);
	}

	public void setClienteSafe(Optional<Cliente> cliente){
		if(cliente.isPresent()){
			this.cliente = cliente.get();
		}else{
			this.cliente = null;
		}
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}
	
	public Optional<Unidade> getUnidadeSafe(){
		if(unidade == null){
			return Optional.empty();
		}
		return Optional.of(unidade);
	}

	public void setUnidadeSafe(Optional<Unidade> unidade){
		if(unidade.isPresent()){
			this.unidade = unidade.get();
		}else{
			this.unidade = null;
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Optional<String> getNomeSafe(){
		if(nome == null){
			return Optional.empty();
		}
		return Optional.of(nome);
	}

	public void setNomeSafe(Optional<String> nome){
		if(nome.isPresent()){
			this.nome = nome.get();
		}else{
			this.nome = null;
		}
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	public Optional<Integer> getQuantidadeSafe(){
		if(quantidade == null){
			return Optional.empty();
		}
		return Optional.of(quantidade);
	}

	public void setQuantidadeSafe(Optional<Integer> quantidade){
		if(quantidade.isPresent()){
			this.quantidade = quantidade.get();
		}else{
			this.quantidade = null;
		}
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	public Optional<BigDecimal> getValorTotalSafe(){
		if(valorTotal == null){
			return Optional.empty();
		}
		return Optional.of(valorTotal);
	}

	public void setValorTotalSafe(Optional<BigDecimal> valorTotal){
		if(valorTotal.isPresent()){
			this.valorTotal = valorTotal.get();
		}else{
			this.valorTotal = null;
		}
	}

	public LocalDate getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(LocalDate dataVenda) {
		this.dataVenda = dataVenda;
	}
	
	public Optional<LocalDate> getDataVendaSafe(){
		if(dataVenda == null){
			return Optional.empty();
		}
		return Optional.of(dataVenda);
	}

	public void setDataVendaSafe(Optional<LocalDate> dataVenda){
		if(dataVenda.isPresent()){
			this.dataVenda = dataVenda.get();
		}else{
			this.dataVenda = null;
		}
	}
	
	
	
}
