package br.edu.toledoprudente.projectJava2023.pojo;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="tamanho")
public class Tamanho extends AbstractEntity<Integer> {

		@NotBlank(message = "Informe o nome")
		@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		
		@PositiveOrZero(message = "Informe uma capacidade total igual ou maior que 0")
		@Column(nullable = false)
		private Integer capacidade;

		@OneToMany(mappedBy = "tamanho")
		private List<Armazem> armazens;	
		
		
		public List<Armazem> getArmazens() {
			return armazens;
		}

		public void setArmazens(List<Armazem> armazens) {
			this.armazens = armazens;
		}
		
		public Optional<List<Armazem>> getArmazensSafe(){
			if(armazens == null){
				return Optional.empty();
			}
			return Optional.of(armazens);
		}

		public void setArmazensSafe(Optional<List<Armazem>> armazens){
			if(armazens.isPresent()){
				this.armazens = armazens.get();
			}else{
				this.armazens = null;
			}
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}
		
		public Optional<String> getNomeSafe(){
			if(nome == null){
				return Optional.empty();
			}
			return Optional.of(nome);
		}

		public void setNomeSafe(Optional<String> nome){
			if(nome.isPresent()){
				this.nome = nome.get();
			}else{
				this.nome = null;
			}
		}
		
		public Integer getCapacidade() {
			return capacidade;
		}

		public void setCapacidade(Integer capacidade) {
			this.capacidade = capacidade;
		}
		
		public Optional<Integer> getCapacidadeSafe(){
			if(capacidade == null){
				return Optional.empty();
			}
			return Optional.of(capacidade);
		}

		public void setCapacidadeSafe(Optional<Integer> capacidade){
			if(capacidade.isPresent()){
				this.capacidade = capacidade.get();
			}else{
				this.capacidade = null;
			}
		}
		
		
		
	
}
