package br.edu.toledoprudente.projectJava2023.pojo;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;
import jakarta.persistence.CascadeType;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="funcionario")
public class Funcionario extends AbstractEntity<Integer> {

	    @NotBlank(message = "Informe o nome")
	    @Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		
	    @NotBlank(message = "Informe o nome de usuário")
		@Column(length = 150, nullable = false)
		private String username;
		
	    @NotBlank(message = "Informe a senha")
		@Column(length = 500, nullable = false)
		private String password;
		
	    @NotNull(message = "Selecione se é administrador")
		@Column(nullable = false)
		private Boolean isAdmin;

		@ManyToOne(cascade=CascadeType.PERSIST)
		@JoinColumn(name="usuario_id")
		private Usuario usuario;
		
		public Usuario getUsuario(){
			return usuario;
		}

		public void setUsuario(Usuario usuario){
			this.usuario = usuario;
		}
		
		public Optional<Usuario> getUsuarioSafe(){
			if(usuario == null){
				return Optional.empty();
			}
			return Optional.of(usuario);
		}

		public void setUsuarioSafe(Optional<Usuario> usuario){
			if(usuario.isPresent()){
				this.usuario = usuario.get();
			}else{
				this.usuario = null;
			}
		}

		public Boolean getIsAdmin() {
			return isAdmin;
		}

		public void setIsAdmin(Boolean isAdmin) {
			this.isAdmin = isAdmin;
		}
		
		public Optional<Boolean> getIsAdminSafe(){
			if(isAdmin == null){
				return Optional.empty();
			}
			return Optional.of(isAdmin);
		}

		public void setIsAdminSafe(Optional<Boolean> isAdmin){
			if(isAdmin.isPresent()){
				this.isAdmin = isAdmin.get();
			}else{
				this.isAdmin = null;
			}
		}
		
		
		
		
		
	
}
