package br.edu.toledoprudente.projectJava2023.pojo;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="categoria")
public class Categoria extends AbstractEntity<Integer> {
        
	    @NotBlank(message = "Informe o nome")
	    @Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		
		@NotBlank(message = "Informe a descriçao")
		@Size(min = 3, max = 150, message = "A descriçao deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String descricao;
		
		@OneToMany(mappedBy = "categoria")
		private List<Artefato> artefatos;
		
		
		
		

		public List<Artefato> getArtefatos() {
			return artefatos;
		}

		public void setArtefatos(List<Artefato> artefatos) {
			this.artefatos = artefatos;
		}
		
		public Optional<List<Artefato>> getArtefatosSafe(){
			if(artefatos == null){
				return Optional.empty();
			}
			return Optional.of(artefatos);
		}

		public void setArtefatosSafe(Optional<List<Artefato>> artefatos){
			if(artefatos.isPresent()){
				this.artefatos = artefatos.get();
			}else{
				this.artefatos = null;
			}
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}
		
		public Optional<String> getNomeSafe(){
			if(nome == null){
				return Optional.empty();
			}
			return Optional.of(nome);
		}

		public void setNomeSafe(Optional<String> nome){
			if(nome.isPresent()){
				this.nome = nome.get();
			}else{
				this.nome = null;
			}
		}
		
		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		public Optional<String> getDescricaoSafe(){
			if(descricao == null){
				return Optional.empty();
			}
			return Optional.of(descricao);
		}

		public void setDescricaoSafe(Optional<String> descricao){
			if(descricao.isPresent()){
				this.descricao = descricao.get();
			}else{
				this.descricao = null;
			}
		}
		
		
		
	
}
