package br.edu.toledoprudente.projectJava2023.pojo;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="artefato")
public class Artefato extends AbstractEntity<Integer> {

	    @NotBlank(message = "Informe o nome")
	    @Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		

		@OneToMany(mappedBy = "artefato")
		private List<Unidade> unidades;
		
		@NotNull(message = "Selecione a categoria")
		@ManyToOne
		@JoinColumn(name="categoria_id")
		private Categoria categoria;
		
		
		
		
		public List<Unidade> getUnidades() {
			return unidades;
		}

		public void getUnidades(List<Unidade> unidades) {
			this.unidades = unidades;
		}
		
		public Optional<List<Unidade>> getUnidadesSafe(){
			if(unidades == null){
				return Optional.empty();
			}
			return Optional.of(unidades);
		}

		public void setUnidadesSafe(Optional<List<Unidade>> unidades){
			if(unidades.isPresent()){
				this.unidades = unidades.get();
			}else{
				this.unidades = null;
			}
		}

		public Categoria getCategoria() {
			return categoria;
		}

		public void setCategoria(Categoria categoria) {
			this.categoria = categoria;
		}
		
		public Optional<Categoria> getCategoriaSafe(){
			if(categoria == null){
				return Optional.empty();
			}
			return Optional.of(categoria);
		}

		public void setCategoriaSafe(Optional<Categoria> categoria){
			if(categoria.isPresent()){
				this.categoria = categoria.get();
			}else{
				this.categoria = null;
			}
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}
		
		public Optional<String> getNomeSafe(){
			if(nome == null){
				return Optional.empty();
			}
			return Optional.of(nome);
		}

		public void setNomeSafe(Optional<String> nome){
			if(nome.isPresent()){
				this.nome = nome.get();
			}else{
				this.nome = null;
			}
		}
		
		
		
	
}
