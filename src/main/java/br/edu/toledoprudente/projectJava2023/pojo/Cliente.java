package br.edu.toledoprudente.projectJava2023.pojo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;
import jakarta.persistence.CascadeType;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="cliente")
public class Cliente extends AbstractEntity<Integer> {

		@NotBlank(message = "Informe o nome")
		@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		
		@NotBlank(message = "Informe o nome de funcionario")
		@Size(min = 3, max = 150, message = "O nome de funcionario deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String username;
		
		@NotBlank(message = "Informe a senha")
		@Column(length = 500, nullable = false)
		private String password;
		
		@PastOrPresent(message = "Selecione a data de nascimento")
		@Column(name="data_nascimento", nullable = false, columnDefinition = "DATE")
		@DateTimeFormat(iso = ISO.DATE)
		private LocalDate dataNascimento;

		@OneToMany(mappedBy = "cliente")
		private List<Venda> vendas;
		
		@ManyToOne(cascade=CascadeType.PERSIST)
		@JoinColumn(name="usuario_id")
		private Usuario usuario;

		public Usuario getUsuario(){
			return usuario;
		}

		public void setUsuario(Usuario usuario){
			this.usuario = usuario;
		}
		
		public Optional<Usuario> getUsuarioSafe(){
			if(usuario == null){
				return Optional.empty();
			}
			return Optional.of(usuario);
		}

		public void setUsuarioSafe(Optional<Usuario> usuario){
			if(usuario.isPresent()){
				this.usuario = usuario.get();
			}else{
				this.usuario = null;
			}
		}
		
		
		public List<Venda> getVendas() {
			return vendas;
		}

		public void setVendas(List<Venda> vendas) {
			this.vendas = vendas;
		}
		
		public Optional<List<Venda>> getVendasSafe() {
			if(vendas == null){
				return Optional.empty();
			}
			return Optional.of(vendas);
		}

		public void setVendasSafe(Optional<List<Venda>> vendas) {
			if(vendas.isPresent()){
				this.vendas = vendas.get();
			}else{
				this.vendas = null;
			}
		}
		
		public LocalDate getDataNascimento() {
			return dataNascimento;
		}

		public void setDataNascimento(LocalDate dataNascimento) {
			this.dataNascimento = dataNascimento;
		}
		
		public Optional<LocalDate> getDataNascimentoSafe(){
			if(dataNascimento == null){
				return Optional.empty();
			}
			return Optional.of(dataNascimento);
		}

		public void setDataNacimentoSafe(Optional<LocalDate> dataNascimento){
			if(dataNascimento.isPresent()){
				this.dataNascimento = dataNascimento.get();
			}else{
				this.dataNascimento = null;
			}
		}
		
		
		
		
		
	
}
