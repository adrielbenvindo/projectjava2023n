package br.edu.toledoprudente.projectJava2023.pojo;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Optional;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
@Entity
@EntityScan
@Table(name="Users")
public class Usuario extends AbstractEntity<Integer> implements UserDetails {
	
	@Column(length = 150, nullable = false)
	private String nome;
	
	@Column(length = 150, nullable = false)
	private String username;
	
	
	@Column(length = 350, nullable = false)
	private String password;
	
	@Column( nullable = false)
	private Boolean enabled;

	@Column( nullable = false)
	private Boolean isCliente;
	
	@OneToMany(mappedBy = "usuario", cascade=CascadeType.PERSIST)
	private List<Cliente> clientes;

	@OneToMany(mappedBy = "usuario", cascade=CascadeType.PERSIST)
	private List<Funcionario> funcionarios;
		
	public List<Cliente> getClientes() {
		return clientes;
	}
	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}
	
	public Optional<List<Cliente>> getClientesSafe(){
		if(clientes == null){
			return Optional.empty();
		}
		return Optional.of(clientes);
	}

	public void setClientesSafe(Optional<List<Cliente>> clientes){
		if(clientes.isPresent()){
			this.clientes = clientes.get();
		}else{
			this.clientes = null;
		}
	}

	public List<Funcionario> getFuncionarios() {
		return funcionarios;
	}
	
	public void setFuncionarios(List<Funcionario> funcionarios) {
		this.funcionarios = funcionarios;
	}
	
	public Optional<List<Funcionario>> getFuncionariosSafe(){
		if(funcionarios == null){
			return Optional.empty();
		}
		return Optional.of(funcionarios);
	}

	public void setFuncionariosSafe(Optional<List<Funcionario>> funcionarios){
		if(funcionarios.isPresent()){
			this.funcionarios = funcionarios.get();
		}else{
			this.funcionarios = null;
		}
	}

	public Boolean getIsCliente(){
		return isCliente;
	}

	public void setIsCliente(Boolean isCliente){
		this.isCliente = isCliente;
	}
	
	public Optional<Boolean> getIsClienteSafe(){
		if(isCliente == null){
			return Optional.empty();
		}
		return Optional.of(isCliente);
	}

	public void setIsClienteSafe(Optional<Boolean> isCliente){
		if(isCliente.isPresent()){
			this.isCliente = isCliente.get();
		}else{
			this.isCliente = null;
		}
	}
	

	public Set<AppAuthority> getAppAuthorities() {
		return appAuthorities;
	}
	
	public void setAppAuthorities(Set<AppAuthority> appAuthorities) {
		this.appAuthorities = appAuthorities;
	}
	
	public Optional<Set<AppAuthority>> getAppAuthoritiesSafe(){
		if(appAuthorities == null){
			return Optional.empty();
		}
		return Optional.of(appAuthorities);
	}

	public void setAppAuthoritiesSafe(Optional<Set<AppAuthority>> appAuthorities){
		if(appAuthorities.isPresent()){
			this.appAuthorities = appAuthorities.get();
		}else{
			this.appAuthorities = null;
		}
	}
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "appUser")
	private Set<AppAuthority> appAuthorities;
	
	public Usuario(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends AppAuthority> authorities//,
	// PersonalInformation personalInformation
	) {
		if (((username == null) || "".equals(username)) || (password == null)) {
			throw new IllegalArgumentException("Cannot pass null or empty values to constructor");
		}
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		// this.accountNonExpired = accountNonExpired;
		// this.credentialsNonExpired = credentialsNonExpired;
		// this.accountNonLocked = accountNonLocked;
		// this.appAuthorities = Collections.unmodifiableSet(sortAuthorities(authorities));
		// this.personalInformation = personalInformation;
	}
	
	public Usuario() {
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
		
	public Optional<String> getNomeSafe(){
		if(nome == null){
			return Optional.empty();
		}
		return Optional.of(nome);
	}

	public void setNomeSafe(Optional<String> nome){
		if(nome.isPresent()){
			this.nome = nome.get();
		}else{
			this.nome = null;
		}
	}

	public Boolean getEnabled() {
		return enabled;
	}
	
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
		
	public Optional<Boolean> getEnabledSafe(){
		if(enabled == null){
			return Optional.empty();
		}
		return Optional.of(enabled);
	}

	public void setEnabledSafe(Optional<Boolean> enabled){
		if(enabled.isPresent()){
			this.enabled = enabled.get();
		}else{
			this.enabled = null;
		}
	}
		
	public String getUsername() {
		return username;
	}
		
	public void setUsername(String username) {
		this.username = username;
	}
		
	public Optional<String> getUsernameSafe(){
		if(username == null){
			return Optional.empty();
		}
		return Optional.of(username);
	}

	public void setUsernameSafe(Optional<String> username){
		if(username.isPresent()){
			this.username = username.get();
		}else{
			this.username = null;
		}
	}
		
	public String getPassword() {
		return password;
	}
		
	public void setPassword(String password) {
		this.password = password;
	}
		
	public Optional<String> getPasswordSafe(){
		if(password == null){
			return Optional.empty();
		}
		return Optional.of(password);
	}

	public void setPasswordSafe(Optional<String> password){
		if(password.isPresent()){
			this.password = password.get();
		}else{
			this.password = null;
		}
	}
		
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}
		
	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}
		
	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}
		
	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}
		
	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}
}

	
