package br.edu.toledoprudente.projectJava2023.pojo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@Table(name = "armazem")
public class Armazem extends AbstractEntity<Integer> {
	
	@NotBlank(message = "Informe o nome")
	@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
	@Column(length = 150, nullable = false)
	private String nome;
	
	@NotNull(message = "Selecione o tamanho")
	@ManyToOne
	@JoinColumn(name="tamanho_id")
	private Tamanho tamanho;
	
	
	public Tamanho getTamanho() {
		return tamanho;
	}

	public void setTamanho(Tamanho tamanho) {
		this.tamanho = tamanho;
	}
	
	public Optional<Tamanho> getTamanhoSafe(){
		if(tamanho == null){
			return Optional.empty();
		}
		return Optional.of(tamanho);
	}

	public void setTamanhoSafe(Optional<Tamanho> tamanho){
		if(tamanho.isPresent()){
			this.tamanho = tamanho.get();
		}else{
			this.tamanho = null;
		}
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Optional<String> getNomeSafe(){
		if(nome == null){
			return Optional.empty();
		}
		return Optional.of(nome);
	}

	public void setNomeSafe(Optional<String> nome){
		if(nome.isPresent()){
			this.nome = nome.get();
		}else{
			this.nome = null;
		}
	}
	
	
	
}
