package br.edu.toledoprudente.projectJava2023.pojo;

import java.math.BigDecimal;
import java.time.LocalDate;

import java.util.List;
import java.util.Optional;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@Table(name = "unidade")
public class Unidade extends AbstractEntity<Integer> {
	
	@NotBlank(message = "Informe o nome")
	@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
	@Column(length = 150, nullable = false)
	private String nome;

	@PositiveOrZero(message = "Informe uma quantidade total igual ou maior que 0")
	@Column(nullable = false)
	private Integer quantidade;
	
	@Column(name="valor", nullable = true, columnDefinition = "DECIMAL(10,2) DEFAULT 0.00")
	@NumberFormat(style = Style.CURRENCY, pattern = "#,##0.00")
	private BigDecimal valor;
	
	@Column(name="image", length = 300)
	private String image;
	
	//@DateTimeFormat(iso = ISO.DATE)
	//private LocalDate data;
	
	@NotNull(message = "Selecione o armazem")
	@ManyToOne
	@JoinColumn(name="armazem_id")
	private Armazem armazem;
	
	@NotNull(message = "Selecione o artefato")
	@ManyToOne
	@JoinColumn(name="artefato_id")
	private Artefato artefato;

	@OneToMany(mappedBy = "unidade")
	private List<Venda> vendas;
	
	

    public List<Venda> getVendas() {
		return vendas;
	}

	public void setVendas(List<Venda> vendas) {
		this.vendas = vendas;
	}
	
	public Optional<List<Venda>> getVendasSafe(){
		if(vendas == null){
			return Optional.empty();
		}
		return Optional.of(vendas);
	}

	public void setVendasSafe(Optional<List<Venda>> vendas){
		if(vendas.isPresent()){
			this.vendas = vendas.get();
		}else{
			this.vendas = null;
		}
	}

	public Armazem getArmazem() {
		return armazem;
	}

	public void setArmazem(Armazem armazem) {
		this.armazem = armazem;
	}
	
	public Optional<Armazem> getArmazemSafe(){
		if(armazem == null){
			return Optional.empty();
		}
		return Optional.of(armazem);
	}

	public void setArmazemSafe(Optional<Armazem> armazem){
		if(armazem.isPresent()){
			this.armazem = armazem.get();
		}else{
			this.armazem = null;
		}
	}

	public Artefato getArtefato() {
		return artefato;
	}

	public void setArtefato(Artefato artefato) {
		this.artefato = artefato;
	}
	
	public Optional<Artefato> getArtefatoSafe(){
		if(artefato == null){
			return Optional.empty();
		}
		return Optional.of(artefato);
	}

	public void setArtefatoSafe(Optional<Artefato> artefato){
		if(artefato.isPresent()){
			this.artefato = artefato.get();
		}else{
			this.artefato = null;
		}
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public Optional<BigDecimal> getValorSafe(){
		if(valor == null){
			return Optional.empty();
		}
		return Optional.of(valor);
	}

	public void setValorSafe(Optional<BigDecimal> valor){
		if(valor.isPresent()){
			this.valor = valor.get();
		}else{
			this.valor = null;
		}
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Optional<String> getNomeSafe(){
		if(nome == null){
			return Optional.empty();
		}
		return Optional.of(nome);
	}

	public void setNomeSafe(Optional<String> nome){
		if(nome.isPresent()){
			this.nome = nome.get();
		}else{
			this.nome = null;
		}
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
	public Optional<Integer> getQuantidadeSafe(){
		if(quantidade == null){
			return Optional.empty();
		}
		return Optional.of(quantidade);
	}

	public void setQuantidadeSafe(Optional<Integer> quantidade){
		if(quantidade.isPresent()){
			this.quantidade = quantidade.get();
		}else{
			this.quantidade = null;
		}
	}
	
    public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public Optional<String> getImageSafe(){
		if(image == null){
			return Optional.empty();
		}
		return Optional.of(image);
	}

	public void setImageSafe(Optional<String> image){
		if(image.isPresent()){
			this.image = image.get();
		}else{
			this.image = null;
		}
	}
	
	
	
	
}
