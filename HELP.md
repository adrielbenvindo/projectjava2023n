ProjectJava2023N/mvnw                                                                               0000755 0001750 0000144 00000024054 14410340616 015540  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  #!/bin/sh
# ----------------------------------------------------------------------------
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#    https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# Maven Start Up Batch script
#
# Required ENV vars:
# ------------------
#   JAVA_HOME - location of a JDK home dir
#
# Optional ENV vars
# -----------------
#   M2_HOME - location of maven2's installed home dir
#   MAVEN_OPTS - parameters passed to the Java VM when running Maven
#     e.g. to debug Maven itself, use
#       set MAVEN_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000
#   MAVEN_SKIP_RC - flag to disable loading of mavenrc files
# ----------------------------------------------------------------------------

if [ -z "$MAVEN_SKIP_RC" ] ; then

  if [ -f /usr/local/etc/mavenrc ] ; then
    . /usr/local/etc/mavenrc
  fi

  if [ -f /etc/mavenrc ] ; then
    . /etc/mavenrc
  fi

  if [ -f "$HOME/.mavenrc" ] ; then
    . "$HOME/.mavenrc"
  fi

fi

# OS specific support.  $var _must_ be set to either true or false.
cygwin=false;
darwin=false;
mingw=false
case "`uname`" in
  CYGWIN*) cygwin=true ;;
  MINGW*) mingw=true;;
  Darwin*) darwin=true
    # Use /usr/libexec/java_home if available, otherwise fall back to /Library/Java/Home
    # See https://developer.apple.com/library/mac/qa/qa1170/_index.html
    if [ -z "$JAVA_HOME" ]; then
      if [ -x "/usr/libexec/java_home" ]; then
        export JAVA_HOME="`/usr/libexec/java_home`"
      else
        export JAVA_HOME="/Library/Java/Home"
      fi
    fi
    ;;
esac

if [ -z "$JAVA_HOME" ] ; then
  if [ -r /etc/gentoo-release ] ; then
    JAVA_HOME=`java-config --jre-home`
  fi
fi

if [ -z "$M2_HOME" ] ; then
  ## resolve links - $0 may be a link to maven's home
  PRG="$0"

  # need this for relative symlinks
  while [ -h "$PRG" ] ; do
    ls=`ls -ld "$PRG"`
    link=`expr "$ls" : '.*-> \(.*\)$'`
    if expr "$link" : '/.*' > /dev/null; then
      PRG="$link"
    else
      PRG="`dirname "$PRG"`/$link"
    fi
  done

  saveddir=`pwd`

  M2_HOME=`dirname "$PRG"`/..

  # make it fully qualified
  M2_HOME=`cd "$M2_HOME" && pwd`

  cd "$saveddir"
  # echo Using m2 at $M2_HOME
fi

# For Cygwin, ensure paths are in UNIX format before anything is touched
if $cygwin ; then
  [ -n "$M2_HOME" ] &&
    M2_HOME=`cygpath --unix "$M2_HOME"`
  [ -n "$JAVA_HOME" ] &&
    JAVA_HOME=`cygpath --unix "$JAVA_HOME"`
  [ -n "$CLASSPATH" ] &&
    CLASSPATH=`cygpath --path --unix "$CLASSPATH"`
fi

# For Mingw, ensure paths are in UNIX format before anything is touched
if $mingw ; then
  [ -n "$M2_HOME" ] &&
    M2_HOME="`(cd "$M2_HOME"; pwd)`"
  [ -n "$JAVA_HOME" ] &&
    JAVA_HOME="`(cd "$JAVA_HOME"; pwd)`"
fi

if [ -z "$JAVA_HOME" ]; then
  javaExecutable="`which javac`"
  if [ -n "$javaExecutable" ] && ! [ "`expr \"$javaExecutable\" : '\([^ ]*\)'`" = "no" ]; then
    # readlink(1) is not available as standard on Solaris 10.
    readLink=`which readlink`
    if [ ! `expr "$readLink" : '\([^ ]*\)'` = "no" ]; then
      if $darwin ; then
        javaHome="`dirname \"$javaExecutable\"`"
        javaExecutable="`cd \"$javaHome\" && pwd -P`/javac"
      else
        javaExecutable="`readlink -f \"$javaExecutable\"`"
      fi
      javaHome="`dirname \"$javaExecutable\"`"
      javaHome=`expr "$javaHome" : '\(.*\)/bin'`
      JAVA_HOME="$javaHome"
      export JAVA_HOME
    fi
  fi
fi

if [ -z "$JAVACMD" ] ; then
  if [ -n "$JAVA_HOME"  ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
      # IBM's JDK on AIX uses strange locations for the executables
      JAVACMD="$JAVA_HOME/jre/sh/java"
    else
      JAVACMD="$JAVA_HOME/bin/java"
    fi
  else
    JAVACMD="`\\unset -f command; \\command -v java`"
  fi
fi

if [ ! -x "$JAVACMD" ] ; then
  echo "Error: JAVA_HOME is not defined correctly." >&2
  echo "  We cannot execute $JAVACMD" >&2
  exit 1
fi

if [ -z "$JAVA_HOME" ] ; then
  echo "Warning: JAVA_HOME environment variable is not set."
fi

CLASSWORLDS_LAUNCHER=org.codehaus.plexus.classworlds.launcher.Launcher

# traverses directory structure from process work directory to filesystem root
# first directory with .mvn subdirectory is considered project base directory
find_maven_basedir() {

  if [ -z "$1" ]
  then
    echo "Path not specified to find_maven_basedir"
    return 1
  fi

  basedir="$1"
  wdir="$1"
  while [ "$wdir" != '/' ] ; do
    if [ -d "$wdir"/.mvn ] ; then
      basedir=$wdir
      break
    fi
    # workaround for JBEAP-8937 (on Solaris 10/Sparc)
    if [ -d "${wdir}" ]; then
      wdir=`cd "$wdir/.."; pwd`
    fi
    # end of workaround
  done
  echo "${basedir}"
}

# concatenates all lines of a file
concat_lines() {
  if [ -f "$1" ]; then
    echo "$(tr -s '\n' ' ' < "$1")"
  fi
}

BASE_DIR=`find_maven_basedir "$(pwd)"`
if [ -z "$BASE_DIR" ]; then
  exit 1;
fi

##########################################################################################
# Extension to allow automatically downloading the maven-wrapper.jar from Maven-central
# This allows using the maven wrapper in projects that prohibit checking in binary data.
##########################################################################################
if [ -r "$BASE_DIR/.mvn/wrapper/maven-wrapper.jar" ]; then
    if [ "$MVNW_VERBOSE" = true ]; then
      echo "Found .mvn/wrapper/maven-wrapper.jar"
    fi
else
    if [ "$MVNW_VERBOSE" = true ]; then
      echo "Couldn't find .mvn/wrapper/maven-wrapper.jar, downloading it ..."
    fi
    if [ -n "$MVNW_REPOURL" ]; then
      jarUrl="$MVNW_REPOURL/org/apache/maven/wrapper/maven-wrapper/3.1.0/maven-wrapper-3.1.0.jar"
    else
      jarUrl="https://repo.maven.apache.org/maven2/org/apache/maven/wrapper/maven-wrapper/3.1.0/maven-wrapper-3.1.0.jar"
    fi
    while IFS="=" read key value; do
      case "$key" in (wrapperUrl) jarUrl="$value"; break ;;
      esac
    done < "$BASE_DIR/.mvn/wrapper/maven-wrapper.properties"
    if [ "$MVNW_VERBOSE" = true ]; then
      echo "Downloading from: $jarUrl"
    fi
    wrapperJarPath="$BASE_DIR/.mvn/wrapper/maven-wrapper.jar"
    if $cygwin; then
      wrapperJarPath=`cygpath --path --windows "$wrapperJarPath"`
    fi

    if command -v wget > /dev/null; then
        if [ "$MVNW_VERBOSE" = true ]; then
          echo "Found wget ... using wget"
        fi
        if [ -z "$MVNW_USERNAME" ] || [ -z "$MVNW_PASSWORD" ]; then
            wget "$jarUrl" -O "$wrapperJarPath" || rm -f "$wrapperJarPath"
        else
            wget --http-user=$MVNW_USERNAME --http-password=$MVNW_PASSWORD "$jarUrl" -O "$wrapperJarPath" || rm -f "$wrapperJarPath"
        fi
    elif command -v curl > /dev/null; then
        if [ "$MVNW_VERBOSE" = true ]; then
          echo "Found curl ... using curl"
        fi
        if [ -z "$MVNW_USERNAME" ] || [ -z "$MVNW_PASSWORD" ]; then
            curl -o "$wrapperJarPath" "$jarUrl" -f
        else
            curl --user $MVNW_USERNAME:$MVNW_PASSWORD -o "$wrapperJarPath" "$jarUrl" -f
        fi

    else
        if [ "$MVNW_VERBOSE" = true ]; then
          echo "Falling back to using Java to download"
        fi
        javaClass="$BASE_DIR/.mvn/wrapper/MavenWrapperDownloader.java"
        # For Cygwin, switch paths to Windows format before running javac
        if $cygwin; then
          javaClass=`cygpath --path --windows "$javaClass"`
        fi
        if [ -e "$javaClass" ]; then
            if [ ! -e "$BASE_DIR/.mvn/wrapper/MavenWrapperDownloader.class" ]; then
                if [ "$MVNW_VERBOSE" = true ]; then
                  echo " - Compiling MavenWrapperDownloader.java ..."
                fi
                # Compiling the Java class
                ("$JAVA_HOME/bin/javac" "$javaClass")
            fi
            if [ -e "$BASE_DIR/.mvn/wrapper/MavenWrapperDownloader.class" ]; then
                # Running the downloader
                if [ "$MVNW_VERBOSE" = true ]; then
                  echo " - Running MavenWrapperDownloader.java ..."
                fi
                ("$JAVA_HOME/bin/java" -cp .mvn/wrapper MavenWrapperDownloader "$MAVEN_PROJECTBASEDIR")
            fi
        fi
    fi
fi
##########################################################################################
# End of extension
##########################################################################################

export MAVEN_PROJECTBASEDIR=${MAVEN_BASEDIR:-"$BASE_DIR"}
if [ "$MVNW_VERBOSE" = true ]; then
  echo $MAVEN_PROJECTBASEDIR
fi
MAVEN_OPTS="$(concat_lines "$MAVEN_PROJECTBASEDIR/.mvn/jvm.config") $MAVEN_OPTS"

# For Cygwin, switch paths to Windows format before running java
if $cygwin; then
  [ -n "$M2_HOME" ] &&
    M2_HOME=`cygpath --path --windows "$M2_HOME"`
  [ -n "$JAVA_HOME" ] &&
    JAVA_HOME=`cygpath --path --windows "$JAVA_HOME"`
  [ -n "$CLASSPATH" ] &&
    CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
  [ -n "$MAVEN_PROJECTBASEDIR" ] &&
    MAVEN_PROJECTBASEDIR=`cygpath --path --windows "$MAVEN_PROJECTBASEDIR"`
fi

# Provide a "standardized" way to retrieve the CLI args that will
# work with both Windows and non-Windows executions.
MAVEN_CMD_LINE_ARGS="$MAVEN_CONFIG $@"
export MAVEN_CMD_LINE_ARGS

WRAPPER_LAUNCHER=org.apache.maven.wrapper.MavenWrapperMain

exec "$JAVACMD" \
  $MAVEN_OPTS \
  $MAVEN_DEBUG_OPTS \
  -classpath "$MAVEN_PROJECTBASEDIR/.mvn/wrapper/maven-wrapper.jar" \
  "-Dmaven.home=${M2_HOME}" \
  "-Dmaven.multiModuleProjectDirectory=${MAVEN_PROJECTBASEDIR}" \
  ${WRAPPER_LAUNCHER} $MAVEN_CONFIG "$@"
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    ProjectJava2023N/mvnw.cmd                                                                           0000644 0001750 0000144 00000015116 14410340616 016276  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  @REM ----------------------------------------------------------------------------
@REM Licensed to the Apache Software Foundation (ASF) under one
@REM or more contributor license agreements.  See the NOTICE file
@REM distributed with this work for additional information
@REM regarding copyright ownership.  The ASF licenses this file
@REM to you under the Apache License, Version 2.0 (the
@REM "License"); you may not use this file except in compliance
@REM with the License.  You may obtain a copy of the License at
@REM
@REM    https://www.apache.org/licenses/LICENSE-2.0
@REM
@REM Unless required by applicable law or agreed to in writing,
@REM software distributed under the License is distributed on an
@REM "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
@REM KIND, either express or implied.  See the License for the
@REM specific language governing permissions and limitations
@REM under the License.
@REM ----------------------------------------------------------------------------

@REM ----------------------------------------------------------------------------
@REM Maven Start Up Batch script
@REM
@REM Required ENV vars:
@REM JAVA_HOME - location of a JDK home dir
@REM
@REM Optional ENV vars
@REM M2_HOME - location of maven2's installed home dir
@REM MAVEN_BATCH_ECHO - set to 'on' to enable the echoing of the batch commands
@REM MAVEN_BATCH_PAUSE - set to 'on' to wait for a keystroke before ending
@REM MAVEN_OPTS - parameters passed to the Java VM when running Maven
@REM     e.g. to debug Maven itself, use
@REM set MAVEN_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000
@REM MAVEN_SKIP_RC - flag to disable loading of mavenrc files
@REM ----------------------------------------------------------------------------

@REM Begin all REM lines with '@' in case MAVEN_BATCH_ECHO is 'on'
@echo off
@REM set title of command window
title %0
@REM enable echoing by setting MAVEN_BATCH_ECHO to 'on'
@if "%MAVEN_BATCH_ECHO%" == "on"  echo %MAVEN_BATCH_ECHO%

@REM set %HOME% to equivalent of $HOME
if "%HOME%" == "" (set "HOME=%HOMEDRIVE%%HOMEPATH%")

@REM Execute a user defined script before this one
if not "%MAVEN_SKIP_RC%" == "" goto skipRcPre
@REM check for pre script, once with legacy .bat ending and once with .cmd ending
if exist "%USERPROFILE%\mavenrc_pre.bat" call "%USERPROFILE%\mavenrc_pre.bat" %*
if exist "%USERPROFILE%\mavenrc_pre.cmd" call "%USERPROFILE%\mavenrc_pre.cmd" %*
:skipRcPre

@setlocal

set ERROR_CODE=0

@REM To isolate internal variables from possible post scripts, we use another setlocal
@setlocal

@REM ==== START VALIDATION ====
if not "%JAVA_HOME%" == "" goto OkJHome

echo.
echo Error: JAVA_HOME not found in your environment. >&2
echo Please set the JAVA_HOME variable in your environment to match the >&2
echo location of your Java installation. >&2
echo.
goto error

:OkJHome
if exist "%JAVA_HOME%\bin\java.exe" goto init

echo.
echo Error: JAVA_HOME is set to an invalid directory. >&2
echo JAVA_HOME = "%JAVA_HOME%" >&2
echo Please set the JAVA_HOME variable in your environment to match the >&2
echo location of your Java installation. >&2
echo.
goto error

@REM ==== END VALIDATION ====

:init

@REM Find the project base dir, i.e. the directory that contains the folder ".mvn".
@REM Fallback to current working directory if not found.

set MAVEN_PROJECTBASEDIR=%MAVEN_BASEDIR%
IF NOT "%MAVEN_PROJECTBASEDIR%"=="" goto endDetectBaseDir

set EXEC_DIR=%CD%
set WDIR=%EXEC_DIR%
:findBaseDir
IF EXIST "%WDIR%"\.mvn goto baseDirFound
cd ..
IF "%WDIR%"=="%CD%" goto baseDirNotFound
set WDIR=%CD%
goto findBaseDir

:baseDirFound
set MAVEN_PROJECTBASEDIR=%WDIR%
cd "%EXEC_DIR%"
goto endDetectBaseDir

:baseDirNotFound
set MAVEN_PROJECTBASEDIR=%EXEC_DIR%
cd "%EXEC_DIR%"

:endDetectBaseDir

IF NOT EXIST "%MAVEN_PROJECTBASEDIR%\.mvn\jvm.config" goto endReadAdditionalConfig

@setlocal EnableExtensions EnableDelayedExpansion
for /F "usebackq delims=" %%a in ("%MAVEN_PROJECTBASEDIR%\.mvn\jvm.config") do set JVM_CONFIG_MAVEN_PROPS=!JVM_CONFIG_MAVEN_PROPS! %%a
@endlocal & set JVM_CONFIG_MAVEN_PROPS=%JVM_CONFIG_MAVEN_PROPS%

:endReadAdditionalConfig

SET MAVEN_JAVA_EXE="%JAVA_HOME%\bin\java.exe"
set WRAPPER_JAR="%MAVEN_PROJECTBASEDIR%\.mvn\wrapper\maven-wrapper.jar"
set WRAPPER_LAUNCHER=org.apache.maven.wrapper.MavenWrapperMain

set DOWNLOAD_URL="https://repo.maven.apache.org/maven2/org/apache/maven/wrapper/maven-wrapper/3.1.0/maven-wrapper-3.1.0.jar"

FOR /F "usebackq tokens=1,2 delims==" %%A IN ("%MAVEN_PROJECTBASEDIR%\.mvn\wrapper\maven-wrapper.properties") DO (
    IF "%%A"=="wrapperUrl" SET DOWNLOAD_URL=%%B
)

@REM Extension to allow automatically downloading the maven-wrapper.jar from Maven-central
@REM This allows using the maven wrapper in projects that prohibit checking in binary data.
if exist %WRAPPER_JAR% (
    if "%MVNW_VERBOSE%" == "true" (
        echo Found %WRAPPER_JAR%
    )
) else (
    if not "%MVNW_REPOURL%" == "" (
        SET DOWNLOAD_URL="%MVNW_REPOURL%/org/apache/maven/wrapper/maven-wrapper/3.1.0/maven-wrapper-3.1.0.jar"
    )
    if "%MVNW_VERBOSE%" == "true" (
        echo Couldn't find %WRAPPER_JAR%, downloading it ...
        echo Downloading from: %DOWNLOAD_URL%
    )

    powershell -Command "&{"^
		"$webclient = new-object System.Net.WebClient;"^
		"if (-not ([string]::IsNullOrEmpty('%MVNW_USERNAME%') -and [string]::IsNullOrEmpty('%MVNW_PASSWORD%'))) {"^
		"$webclient.Credentials = new-object System.Net.NetworkCredential('%MVNW_USERNAME%', '%MVNW_PASSWORD%');"^
		"}"^
		"[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; $webclient.DownloadFile('%DOWNLOAD_URL%', '%WRAPPER_JAR%')"^
		"}"
    if "%MVNW_VERBOSE%" == "true" (
        echo Finished downloading %WRAPPER_JAR%
    )
)
@REM End of extension

@REM Provide a "standardized" way to retrieve the CLI args that will
@REM work with both Windows and non-Windows executions.
set MAVEN_CMD_LINE_ARGS=%*

%MAVEN_JAVA_EXE% ^
  %JVM_CONFIG_MAVEN_PROPS% ^
  %MAVEN_OPTS% ^
  %MAVEN_DEBUG_OPTS% ^
  -classpath %WRAPPER_JAR% ^
  "-Dmaven.multiModuleProjectDirectory=%MAVEN_PROJECTBASEDIR%" ^
  %WRAPPER_LAUNCHER% %MAVEN_CONFIG% %*
if ERRORLEVEL 1 goto error
goto end

:error
set ERROR_CODE=1

:end
@endlocal & set ERROR_CODE=%ERROR_CODE%

if not "%MAVEN_SKIP_RC%"=="" goto skipRcPost
@REM check for post script, once with legacy .bat ending and once with .cmd ending
if exist "%USERPROFILE%\mavenrc_post.bat" call "%USERPROFILE%\mavenrc_post.bat"
if exist "%USERPROFILE%\mavenrc_post.cmd" call "%USERPROFILE%\mavenrc_post.cmd"
:skipRcPost

@REM pause the script if MAVEN_BATCH_PAUSE is set to 'on'
if "%MAVEN_BATCH_PAUSE%"=="on" pause

if "%MAVEN_TERMINATE_CMD%"=="on" exit %ERROR_CODE%

cmd /C exit /B %ERROR_CODE%
                                                                                                                                                                                                                                                                                                                                                                                                                                                  ProjectJava2023N/pom.xml                                                                            0000644 0001750 0000144 00000004343 14414643470 016147  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  <?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.0.5</version>
		<relativePath/> <!-- lookup parent from repository -->
	</parent>
	<groupId>br.edu.toledoprudente</groupId>
	<artifactId>ProjectJava2023N</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>ProjectJava2023N</name>
	<description>Demo project for Spring Boot</description>
	<properties>
		<java.version>17</java.version>
	</properties>
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
			<optional>true</optional>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>mysql</groupId>
			<artifactId>mysql-connector-java</artifactId>
			<version>8.0.19</version>
		</dependency>
		<dependency>
			<groupId>jakarta.validation</groupId>
			<artifactId>jakarta.validation-api</artifactId>
			<version>3.0.2</version>
		</dependency>
		<dependency>
  			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-validation</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
			</plugin>
		</plugins>
	</build>

</project>
                                                                                                                                                                                                                                                                                             ProjectJava2023N/src/                                                                               0000755 0001750 0000144 00000000000 14410340616 015405  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/                                                                          0000755 0001750 0000144 00000000000 14410340616 016331  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/java/                                                                     0000755 0001750 0000144 00000000000 14410340616 017252  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/java/br/                                                                  0000755 0001750 0000144 00000000000 14410340616 017655  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/java/br/edu/                                                              0000755 0001750 0000144 00000000000 14410340616 020432  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/java/br/edu/toledoprudente/                                               0000755 0001750 0000144 00000000000 14410347422 023471  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/                               0000755 0001750 0000144 00000000000 14410347422 026250  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ././@LongLink                                                                                       0000644 0000000 0000000 00000000146 00000000000 011604  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/ProjectJava2023NApplication.java                                                                                                                                                                                                                                                                                                                                                                                                                           ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/ProjectJava2023NApplication.jav0000644 0001750 0000144 00000000536 14410340616 033775  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectJava2023NApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectJava2023NApplication.class, args);
	}

}
                                                                                                                                                                  ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/                    0000755 0001750 0000144 00000000000 14414647127 030444  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ././@LongLink                                                                                       0000644 0000000 0000000 00000000163 00000000000 011603  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/.CategoriaController.java.kate-swp                                                                                                                                                                                                                                                                                                                                                                                                              ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/.CategoriaController0000600 0001750 0000144 00000003454 14406164677 034412  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                     Kate Swap File 2.0   a@ז˿� ����?�X�SW   5   I   6       	ESW   6   I   7       	ESI   7      GESI   7      eESI   7      tESI   7      MESI   7      aESI   7      pESI   7      pESI   7      iESI   7   	   nESI   7      @ESI   7      gESI   7      (I   7      )ESI   7      "I   7      "ESI   7      /ESI   7      sESI   7      aESI   7      lESI   7      vESI   7      aESI   7      rESW   7   I   8       	ESR   8       ESU   8ESR   7      ESR   7      ESR   7      ESR   7      ESR   7      ESR   7      ESI   7      lESI   7      iESI   7      sESI   7      tESI   7      aESI   7      rESW   7   I   8       	ESI   8      pESI   8      uESI   8      bESI   8      lESI   8      iESI   8      cESI   8       ESI   8      SESI   8   	   rESR   8   	   
ESI   8   	   tESI   8   
   rESI   8      iESI   8      nESI   8      gESI   8       ESI   8      lESI   8      iESI   8      sESI   8      tESI   8      aESI   8      rESI   8      (I   8      )ESI   8      {I   8      }ESW   8   I   9       	ESW   9   I   :       	ESI   9       ESI   9       ESI   9       ESR   9       I   9       	ESR   9       ESI   9        ESI   9       ESI   9       ESI   9       ESR   9       I   9       		ESI   9      rESI   9      eESI   9      tESI   9      uESI   9      rESI   9      nESI   9       ESI   9   	   "I   9   
   "ESI   9      ;ESI   9   
   /ESI   9      cESI   9      aESI   9      tESI   9      eESI   9      gESI   9      oESI   9      rESI   9      iESI   9      aESI   9      /ESI   9      lESI   9      iESI   9      sESI   9      tESW   8   I   9       	ESI   9       ESI   9       ESI   9       ESI   9       E                                                                                                                                                                                                                    ././@LongLink                                                                                       0000644 0000000 0000000 00000000156 00000000000 011605  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/StringToTamanhoConverter.java                                                                                                                                                                                                                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/StringToTamanhoConve0000644 0001750 0000144 00000001233 14410423214 034423  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import br.edu.toledoprudente.projectJava2023.dao.TamanhoDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Tamanho;

@Component
public class StringToTamanhoConverter implements Converter <String, Tamanho> {

	@Autowired
	private TamanhoDAO dao;

	@Override
	public Tamanho convert(String idText) {
		// TODO Auto-generated method stub
		if(idText.isEmpty())
			return null;

		return dao.findById(Integer.parseInt( idText));
	}
}
                                                                                                                                                                                                                                                                                                                                                                     ././@LongLink                                                                                       0000644 0000000 0000000 00000000157 00000000000 011606  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/StringToArtefatoConverter.java                                                                                                                                                                                                                                                                                                                                                                                                                  ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/StringToArtefatoConv0000644 0001750 0000144 00000001241 14410423720 034435  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import br.edu.toledoprudente.projectJava2023.dao.ArtefatoDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Artefato;

@Component
public class StringToArtefatoConverter implements Converter <String, Artefato> {

	@Autowired
	private ArtefatoDAO dao;

	@Override
	public Artefato convert(String idText) {
		// TODO Auto-generated method stub
		if(idText.isEmpty())
			return null;

		return dao.findById(Integer.parseInt( idText));
	}
}
                                                                                                                                                                                                                                                                                                                                                               ././@LongLink                                                                                       0000644 0000000 0000000 00000000156 00000000000 011605  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/StringToArmazemConverter.java                                                                                                                                                                                                                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/StringToArmazemConve0000644 0001750 0000144 00000001233 14410423734 034437  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import br.edu.toledoprudente.projectJava2023.dao.ArmazemDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Armazem;

@Component
public class StringToArmazemConverter implements Converter <String, Armazem> {

	@Autowired
	private ArmazemDAO dao;

	@Override
	public Armazem convert(String idText) {
		// TODO Auto-generated method stub
		if(idText.isEmpty())
			return null;

		return dao.findById(Integer.parseInt( idText));
	}
}
                                                                                                                                                                                                                                                                                                                                                                     ././@LongLink                                                                                       0000644 0000000 0000000 00000000147 00000000000 011605  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/ArmazemController.java                                                                                                                                                                                                                                                                                                                                                                                                                          ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/ArmazemController.ja0000644 0001750 0000144 00000006377 14410670175 034430  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.TamanhoDAO;
import br.edu.toledoprudente.projectJava2023.dao.ArmazemDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Tamanho;
import br.edu.toledoprudente.projectJava2023.pojo.Armazem;

@Controller
@RequestMapping("/armazem")
public class ArmazemController {
	
	
	@Autowired
	private ArmazemDAO dao;
	
	@Autowired
	private TamanhoDAO dao_tamanho;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("armazem", new Armazem());
		return "/armazem/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/armazem/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		model.addAttribute("armazem", dao.findById(id));
		
		return "/armazem/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			dao.delete(id);
			model.addAttribute("mensagem", "Exclusão efetuada");
			model.addAttribute("retorno",true);
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/armazem/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("cliente") Armazem armazem, ModelMap model) {
		try {
		    Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Armazem>> constraintViolations = validator.validate( armazem );
            String errors = "";
            for (ConstraintViolation<Armazem> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("armazem", armazem);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/armazem/index";
            }else{
				if(armazem.getId()==null)
					dao.save(armazem);
				else
					dao.update(armazem);
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno",false);
		}
		
		return "/cliente/index";
	}
	/*método usado pra retornar dados para select html*/
	@ModelAttribute(name = "list_tamanho")
	public List<Tamanho> listTamanho(){
		return dao_tamanho.findAll();
		
	}
	
}
                                                                                                                                                                                                                                                                 ././@LongLink                                                                                       0000644 0000000 0000000 00000000151 00000000000 011600  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/CategoriaController.java                                                                                                                                                                                                                                                                                                                                                                                                                        ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/CategoriaController.0000644 0001750 0000144 00000006020 14410670234 034374  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.CategoriaDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Categoria;

@Controller
@RequestMapping("/categoria")
public class CategoriaController {
	
	
	@Autowired
	private CategoriaDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("categoria", 
				new Categoria());
		return "/categoria/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/categoria/list";
	}
	
	@GetMapping("/change")
	public String change_c()@RequestParam(name="id") int id, ModelMap model) {
		model.addAttribute("categoria",
				dao.findById(id));
		
		return "/categoria/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			dao.delete(id);
			model.addAttribute("mensagem", "Exclusão efetuada");
			model.addAttribute("retorno",true);
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/categoria/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(
			@ModelAttribute("categoria") 
			Categoria categoria, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Categoria>> constraintViolations = validator.validate( categoria );
            String errors = "";
            for (ConstraintViolation<Categoria> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("categoria", categoria);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/categoria/index";
            }else{
				if(categoria.getId()==null)
					dao.save(categoria);
				else
					dao.update(categoria);
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/categoria/index";
	}
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ././@LongLink                                                                                       0000644 0000000 0000000 00000000147 00000000000 011605  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/ClienteController.java                                                                                                                                                                                                                                                                                                                                                                                                                          ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/ClienteController.ja0000644 0001750 0000144 00000005722 14410670307 034405  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.ClienteDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Cliente;

@Controller
@RequestMapping("/Cliente")
public class ClienteController {
	
	
	@Autowired
	private ClienteDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("cliente", new Cliente());
		return "/cliente/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/cliente/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		model.addAttribute("cliente", dao.findById(id));
		
		return "/cliente/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			dao.delete(id);
			model.addAttribute("mensagem", "Exclusão efetuada");
			model.addAttribute("retorno",true);
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/cliente/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(
			@ModelAttribute("cliente") 
			Cliente cliente, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Cliente>> constraintViolations = validator.validate( cliente );
            String errors = "";
            for (ConstraintViolation<Cliente> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("cliente", cliente);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/cliente/index";
            }else{
				if(cliente.getId()==null)
					dao.save(cliente);
				else
					dao.update(cliente);
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/cliente/index";
	}
	
}
                                              ././@LongLink                                                                                       0000644 0000000 0000000 00000000147 00000000000 011605  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/TamanhoController.java                                                                                                                                                                                                                                                                                                                                                                                                                          ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/TamanhoController.ja0000644 0001750 0000144 00000005736 14410670360 034415  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.TamanhoDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Tamanho;

@Controller
@RequestMapping("/tamanho")
public class TamanhoController {
	
	
	@Autowired
	private TamanhoDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("tamanho", new Tamanho());
		return "/tamanho/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/tamanho/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		model.addAttribute("tamanho", dao.findById(id));
		
		return "/tamanho/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			dao.delete(id);
			model.addAttribute("mensagem",
					"Exclusão efetuada");
			model.addAttribute("retorno",true);
		}
		catch (Exception e) {
			model.addAttribute("mensagem",
					"Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/tamanho/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(
			@ModelAttribute("tamanho") 
			Tamanho tamanho, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Tamanho>> constraintViolations = validator.validate( tamanho );
            String errors = "";
            for (ConstraintViolation<Tamanho> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("tamanho", tamanho);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/tamanho/index";
            }else{
				if(tamanho.getId()==null)
					dao.save(tamanho);
				else
					dao.update(tamanho);
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/tamanho/index";
	}
	
}
                                  ././@LongLink                                                                                       0000644 0000000 0000000 00000000150 00000000000 011577  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/ArtefatoController.java                                                                                                                                                                                                                                                                                                                                                                                                                         ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/ArtefatoController.j0000644 0001750 0000144 00000005737 14410670502 034431  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.ArtefatoDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Artefato;

@Controller
@RequestMapping("/artefato")
public class ArtefatoController {
	
	
	@Autowired
	private ArtefatoDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("artefato", new Artefato());
		return "/artefato/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/artefato/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		model.addAttribute("artefato", dao.findById(id));
		
		return "/artefato/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			dao.delete(id);
			model.addAttribute("mensagem", "Exclusão efetuada");
			model.addAttribute("retorno",true);
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/artefato/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(
			@ModelAttribute("artefato") 
			Artefato artefato, ModelMap model) {
		try {
		    Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Artefato>> constraintViolations = validator.validate( artefato );
            String errors = "";
            for (ConstraintViolation<Artefato> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("artefato",artefato);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/artefato/index";
            }else{
				if(artefato.getId()==null)
					dao.save(artefato);
				else
					dao.update(artefato);
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/artefato/index";
	}
	
}
                                 ././@LongLink                                                                                       0000644 0000000 0000000 00000000147 00000000000 011605  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/UsuarioController.java                                                                                                                                                                                                                                                                                                                                                                                                                          ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/UsuarioController.ja0000644 0001750 0000144 00000005677 14410670725 034466  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.UsuarioDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Usuario;

@Controller
@RequestMapping("/tamanho")
public class UsuarioController {
	
	
	@Autowired
	private UsuarioDAO dao;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("usuario", new Usuario());
		return "/usuario/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list",
				dao.findAll());
		return "/usuario/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		model.addAttribute("tamanho", dao.findById(id));
		
		return "/usuario/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			dao.delete(id);
			model.addAttribute("mensagem", "Exclusão efetuada");
			model.addAttribute("retorno",true);
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("lista", dao.findAll());
		return "/usuario/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("usuario") Usuario usuario, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Usuario>> constraintViolations = validator.validate( usuario );
            String errors = "";
            for (ConstraintViolation<Usuario> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("usuario",usuario);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/usuario/index";
            }else{
				if(usuario.getId()==null)
					dao.save(usuario);
				else
					dao.update(usuario);
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/usuario/index";
	}
	
}
                                                                 ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/VendaController.java0000644 0001750 0000144 00000006752 14410671105 034407  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.UnidadeDAO;
import br.edu.toledoprudente.projectJava2023.dao.ClienteDAO;
import br.edu.toledoprudente.projectJava2023.dao.VendaDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Unidade;
import br.edu.toledoprudente.projectJava2023.pojo.Cliente;
import br.edu.toledoprudente.projectJava2023.pojo.Venda;

@Controller
@RequestMapping("/venda")
public class VendaController {
	
	
	@Autowired
	private VendaDAO dao;
	
	@Autowired
	private UnidadeDAO dao_unidade;
	
	@Autowired
	private ClienteDAO dao_cliente;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("venda", new Venda());
		return "/venda/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/venda/list";
	}
	
	@GetMapping("/change")
	public String change_c(
		@RequestParam(name="id") int id, 
		ModelMap model) {
		model.addAttribute("venda", dao.findById(id));
		
		return "/venda/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			dao.delete(id);
			model.addAttribute("mensagem", "Exclusão efetuada");
			model.addAttribute("retorno",true);
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/venda/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("venda") Venda venda, ModelMap model) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Venda>> constraintViolations = validator.validate( venda );
            String errors = "";
            for (ConstraintViolation<Venda> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("venda",venda);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/venda/index";
            }else{
				if(venda.getId()==null)
					dao.save(venda);
				else
					dao.update(venda);
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/venda/index";
	}
	/*método usado pra retornar dados para select html*/
	@ModelAttribute(name = "list_unidade")
	public List<Unidade> listUnidade(){
		return dao_unidade.findAll();
		
	}
	
	@ModelAttribute(name = "list_cliente")
	public List<Cliente> listCliente(){
		return dao_cliente.findAll();
		
	}
	
}
                      ././@LongLink                                                                                       0000644 0000000 0000000 00000000147 00000000000 011605  L                                                                                                    ustar   root                            root                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/UnidadeController.java                                                                                                                                                                                                                                                                                                                                                                                                                          ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/controller/UnidadeController.ja0000644 0001750 0000144 00000012353 14414647127 034401  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;
import java.util.Set;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.edu.toledoprudente.projectJava2023.dao.ArmazemDAO;
import br.edu.toledoprudente.projectJava2023.dao.CategoriaDAO;
import br.edu.toledoprudente.projectJava2023.dao.ArtefatoDAO;
import br.edu.toledoprudente.projectJava2023.dao.UnidadeDAO;
import br.edu.toledoprudente.projectJava2023.pojo.Armazem;
import br.edu.toledoprudente.projectJava2023.pojo.Categoria;
import br.edu.toledoprudente.projectJava2023.pojo.Artefato;
import br.edu.toledoprudente.projectJava2023.pojo.Unidade;

@Controller
@RequestMapping("/unidade")
public class UnidadeController {
	
	
	@Autowired
	private UnidadeDAO dao;
	
	@Autowired
	private ArmazemDAO dao_armazem;
	
	@Autowired
	private CategoriaDAO dao_categoria;
	
	@Autowired
	private ArtefatoDAO dao_artefato;
	
	@GetMapping("/new")
	public String new_c(ModelMap model) {
		model.addAttribute("unidade", new Unidade());
		return "/unidade/index";
	}
	
	
	@GetMapping("/list")
	public String list_c(ModelMap model) {
		model.addAttribute("list", dao.findAll());
		return "/unidade/list";
	}
	
	@GetMapping("/change")
	public String change_c(@RequestParam(name="id") int id, ModelMap model) {
		model.addAttribute("unidade", dao.findById(id));
		
		return "/unidade/index";
	}
	
	@GetMapping("/delete")
	public String delete_c(@RequestParam(name="id")int id, ModelMap model) {
		try {
			dao.delete(id);
			model.addAttribute("mensagem", "Exclusão efetuada");
			model.addAttribute("retorno",true);
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Exclusão não pode ser efetuada!");
			model.addAttribute("retorno",false);
		}
		model.addAttribute("list", dao.findAll());
		return "/unidade/list";
	}
	
	
	@PostMapping("/save")
	public String save_c(@ModelAttribute("unidade") Unidade unidade, ModelMap model, @RequestParam("file") MultipartFile file) {
		try {
			Validator validator;
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            validator = factory.getValidator();
            Set<ConstraintViolation<Unidade>> constraintViolations = validator.validate( unidade );
            String errors = "";
            for (ConstraintViolation<Unidade> constraintViolation : constraintViolations) {
            	errors = errors + constraintViolation.getMessage() + ". "; 
            }
            if(errors!=""){
            	//tem erros
				model.addAttribute("unidade", unidade);
				model.addAttribute("mensagem", errors);
				model.addAttribute("retorno", false);
				return "/unidade/index";
            }else{
            	Random random = new Random();
				String fileName = random.nextInt() + file.getOriginalFilename();
			    unidade.setImage(fileName);
				if(unidade.getId()==null)
					dao.save(unidade);
				else
					dao.update(unidade);
				try {        
					byte[] bytes = file.getBytes();
            		Path path = Paths.get(System.getProperty("user.dir") + "\\src\\main\\resources\\static\\image\\" + fileName);
            		Files.write(path, bytes);
        		}catch (IOException e){
            		e.printStackTrace();        
                }
				model.addAttribute("mensagem", "Salvo com sucesso!");
				model.addAttribute("retorno", true);
            }
		}
		catch (Exception e) {
			model.addAttribute("mensagem", "Erro ao salvar!" + e.getMessage());
			model.addAttribute("retorno", false);
		}
		
		return "/unidade/index";
	}
	/*método usado pra retornar dados para select html*/
	@ModelAttribute(name = "list_armazem")
	public List<Armazem> listArmazem(){
		return dao_armazem.findAll();
		
	}
	
	@ModelAttribute(name = "list_artefato")
	public List<Artefato> listArtefato(){
		return dao_artefato.listAllRelated();
		
	}
	
	@ResponseBody
	@RequestMapping(value = "/getimage/{name}", method = RequestMethod.GET)
	public HttpEntity<byte[]> download(@PathVariable(value = "name") String name) throws IOException {
		byte[] file = Files.readAllBytes( Paths.get(System.getProperty("user.dir") +"\\src\\main\\resources\\static\\image\\" + name));
		HttpHeaders httpHeaders = new HttpHeaders();
		switch (name.substring(name.lastIndexOf(".") + 1).toUpperCase()) {
    		case "JPG":
    			httpHeaders.setContentType(MediaType.IMAGE_JPEG);
    			break;
			case "GIF":
				httpHeaders.setContentType(MediaType.IMAGE_GIF); 
				break;
			case "PNG":
				httpHeaders.setContentType(MediaType.IMAGE_PNG); 
				break;
			default:
				break;
		}        
		httpHeaders.setContentLength(file.length);
		HttpEntity<byte[]> entity = new HttpEntity<byte[]>( file, httpHeaders);
		return entity;
	}
}
                                                                                                                                                                                                                                                                                     ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/                           0000755 0001750 0000144 00000000000 14410355451 027014  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/AbstractDao.java           0000644 0001750 0000144 00000003025 14410352354 032045  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;



public abstract class AbstractDao<T, PK extends Serializable> {

	@SuppressWarnings("unchecked")
	private final Class<T> entityClass = 
			(Class<T>) ( (ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	
	@PersistenceContext
	private EntityManager entityManager;

	protected EntityManager getEntityManager() {
		return entityManager;
	}
	
	@Transactional
	public void save(T entity) { 

		entityManager.persist(entity);
	}
	
	@Transactional
	public void update(T entity) {
		
		entityManager.merge(entity);
	}
	
	@Transactional
	public void delete(PK id) {
		
		entityManager.remove(entityManager.getReference(entityClass, id));
	}
	
	public T findById(PK id) {
		
		return entityManager.find(entityClass, id);
	}
	
	public List<T> findAll() {
		
		return entityManager
				.createQuery("from " + entityClass.getSimpleName(), entityClass)
				.getResultList();
	}	
	
	protected List<T> createQuery(String jpql, Object... params) {
		TypedQuery<T> query = entityManager.createQuery(jpql, entityClass);
		for (int i = 0; i < params.length; i++) {
		    query.setParameter(i+1, params[i]);
        }
    	return query.getResultList();
	}
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/TamanhoDAO.java            0000644 0001750 0000144 00000000400 14410352307 031561  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Tamanho;

@Repository
public class TamanhoDAO 
extends AbstractDao<Tamanho, 
Integer> {

}
                                                                                                                                                                                                                                                                ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/ArmazemDAO.java            0000644 0001750 0000144 00000000374 14410352236 031601  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Armazem;

@Repository
public class ArmazemDAO extends AbstractDao<Armazem, Integer> {

}
                                                                                                                                                                                                                                                                    ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/CategoriaDAO.java          0000644 0001750 0000144 00000000406 14410355033 032075  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Categoria;

@Repository
public class CategoriaDAO 
extends AbstractDao<Categoria, 
Integer> {

}
                                                                                                                                                                                                                                                          ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/ArtefatoDAO.java           0000644 0001750 0000144 00000000623 14410421001 031732  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Artefato;

@Repository
public class ArtefatoDAO 
extends AbstractDao<Artefato, 
Integer> {
	public List<Artefato> listAllRelated(){
		return createQuery("select a from Artefato a join fetch a.categoria");
	}
}
                                                                                                             ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/UsuarioDAO.java            0000644 0001750 0000144 00000000400 14410354765 031633  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Usuario;

@Repository
public class UsuarioDAO 
extends AbstractDao<Usuario, 
Integer> {

}
                                                                                                                                                                                                                                                                ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/ClienteDAO.java            0000644 0001750 0000144 00000000400 14410355264 031562  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Cliente;

@Repository
public class ClienteDAO 
extends AbstractDao<Cliente, 
Integer> {

}
                                                                                                                                                                                                                                                                ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/UnidadeDAO.java            0000644 0001750 0000144 00000000374 14410355636 031565  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Unidade;

@Repository
public class UnidadeDAO extends AbstractDao<Unidade, Integer> {

}
                                                                                                                                                                                                                                                                    ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/dao/VendaDAO.java              0000644 0001750 0000144 00000000366 14410355603 031244  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.dao;

import org.springframework.stereotype.Repository;

import br.edu.toledoprudente.projectJava2023.pojo.Venda;

@Repository
public class VendaDAO extends AbstractDao<Venda, Integer> {

}
                                                                                                                                                                                                                                                                          ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/                          0000755 0001750 0000144 00000000000 14414646325 027227  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/AbstractEntity.java       0000644 0001750 0000144 00000002245 14410352333 033022  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.pojo;

import java.io.Serializable;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;



@SuppressWarnings("serial")
@MappedSuperclass
public abstract class AbstractEntity<ID extends Serializable> implements Serializable {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private ID id;

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractEntity<?> other = (AbstractEntity<?>) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "id = " + id;
	}	
}
                                                                                                                                                                                                                                                                                                                                                           ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/Usuario.java              0000644 0001750 0000144 00000003455 14410665003 031516  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.pojo;

import java.util.List;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="usuario")
public class Usuario 
extends AbstractEntity<Integer> {

	    @NotBlank(message = "Informe o nome")
	    @Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		
	    @NotBlank(message = "Informe o nome de usuário")
		@Column(length = 150, nullable = false)
		private String username;
		
	    @NotBlank(message = "Informe a senha")
		@Column(length = 500, nullable = false)
		private String password;
		
	    @NotNull(message = "Selecione se é administrador")
		@Column(nullable = false)
		private Boolean isAdmin;

		@OneToMany(mappedBy = "categoria")
		private List<Armazem> clientes;
		
		
		
		
		public List<Armazem> getClientes() {
			return clientes;
		}

		public void setClientes(List<Armazem> clientes) {
			this.clientes = clientes;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public Boolean getIsAdmin() {
			return isAdmin;
		}

		public void setIsAdmin(Boolean isAdmin) {
			this.isAdmin = isAdmin;
		}
		
		
		
		
		
	
}
                                                                                                                                                                                                                   ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/Categoria.java            0000644 0001750 0000144 00000003165 14410665010 031761  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.pojo;

import java.util.List;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="categoria")
public class Categoria 
extends AbstractEntity<Integer> {
        
	    @NotBlank(message = "Informe o nome")
	    @Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		
		@NotBlank(message = "Informe a descriçao")
		@Size(min = 3, max = 150, message = "A descriçao deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String descricao;

		@OneToMany(mappedBy = "categoria")
		private List<Armazem> clientes;
		
		@OneToMany(mappedBy = "categoria")
		private List<Artefato> artefatos;
		
		
		
		
		public List<Armazem> getClientes() {
			return clientes;
		}

		public void setClientes(List<Armazem> clientes) {
			this.clientes = clientes;
		}

		public List<Artefato> getArtefatos() {
			return artefatos;
		}

		public void setArtefatos(List<Artefato> artefatos) {
			this.artefatos = artefatos;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}
		
		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		
		
		
	
}
                                                                                                                                                                                                                                                                                                                                                                                                           ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/Artefato.java             0000644 0001750 0000144 00000002573 14410665011 031633  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.pojo;

import java.util.List;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="artefato")
public class Artefato 
extends AbstractEntity<Integer> {

	    @NotBlank(message = "Informe o nome")
	    @Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		

		@OneToMany(mappedBy = "artefato")
		private List<Armazem> clientes;
		
		@NotNull(message = "Selecione a categoria")
		@ManyToOne
		@JoinColumn(name="categoria_id")
		private Categoria categoria;
		
		
		
		
		public List<Armazem> getClientes() {
			return clientes;
		}

		public void setClientes(List<Armazem> clientes) {
			this.clientes = clientes;
		}

		public Categoria getCategoria() {
			return categoria;
		}

		public void setCategoria(Categoria categoria) {
			this.categoria = categoria;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}
		
		
		
	
}
                                                                                                                                     ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/Armazem.java              0000644 0001750 0000144 00000002066 14410665012 031460  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.pojo;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@Table(name = "armazem")
public class Armazem extends AbstractEntity<Integer> {
	
	@NotBlank(message = "Informe o nome")
	@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
	@Column(length = 150, nullable = false)
	private String nome;
	
	@NotNull(message = "Selecione o tamanho")
	@ManyToOne
	@JoinColumn(name="tamanho_id")
	private Tamanho tamanho;
	
	
	public Tamanho getTamanho() {
		return tamanho;
	}

	public void setTamanho(Tamanho tamanho) {
		this.tamanho = tamanho;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
	
}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/Venda.java                0000644 0001750 0000144 00000004537 14410665513 031134  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.pojo;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@Table(name = "venda")
public class Venda extends AbstractEntity<Integer> {
	
	@NotBlank(message = "Informe o nome")
	@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
	@Column(length = 150, nullable = false)
	private String nome;

	@PositiveOrZero(message = "Informe uma quantidade total igual ou maior que 0")
	@Column(nullable = false)
	private Integer quantidade;
	
	@PositiveOrZero(message = "Informe um valor total igual ou maior que 0")
	@Column(name="valor_total", nullable = true, columnDefinition = "DECIMAL(10,2) DEFAULT 0.00")
	private BigDecimal valorTotal;
	
	@PastOrPresent(message = "Selecione uma data de venda")
	@Column(name="data_venda", nullable = false, columnDefinition = "DATE")
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate dataVenda;
	
	@NotNull(message = "Selecione o cliente")
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;
	
	@NotNull(message = "Selecione a unidade")
	@ManyToOne
	@JoinColumn(name="unidade_id")
	private Unidade unidade;
	
	

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public LocalDate getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(LocalDate dataVenda) {
		this.dataVenda = dataVenda;
	}
	
	
	
}
                                                                                                                                                                 ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/Cliente.java              0000644 0001750 0000144 00000004230 14410665547 031457  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.pojo;

import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="cliente")
public class Cliente 
extends AbstractEntity<Integer> {

		@NotBlank(message = "Informe o nome")
		@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		
		@NotBlank(message = "Informe o nome de usuario")
		@Size(min = 3, max = 150, message = "O nome de usuario deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String username;
		
		@NotBlank(message = "Informe a senha")
		@Column(length = 500, nullable = false)
		private String password;
		
		@PastOrPresent(message = "Selecione a data de nascimento")
		@Column(name="data_nascimento", nullable = false, columnDefinition = "DATE")
		@DateTimeFormat(iso = ISO.DATE)
		private LocalDate dataNascimento;

		@OneToMany(mappedBy = "cliente")
		private List<Venda> vendas;
		
		
		
		
		public List<Venda> getVendas() {
			return vendas;
		}

		public void setVendas(List<Venda> vendas) {
			this.vendas = vendas;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
		
		public LocalDate getDataNascimento() {
			return dataNascimento;
		}

		public void setDataNascimento(LocalDate dataNascimento) {
			this.dataNascimento = dataNascimento;
		}
		
		
		
		
		
	
}
                                                                                                                                                                                                                                                                                                                                                                        ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/Tamanho.java              0000644 0001750 0000144 00000002457 14410665671 031472  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.pojo;

import java.util.List;

import org.springframework.boot.autoconfigure.domain.EntityScan;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@EntityScan
@Table(name="tamanho")
public class Tamanho 
extends AbstractEntity<Integer> {

		@NotBlank(message = "Informe o nome")
		@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
		@Column(length = 150, nullable = false)
		private String nome;
		
		@PositiveOrZero(message = "Informe uma capacidade total igual ou maior que 0")
		@Column(nullable = false)
		private Integer capacidade;

		@OneToMany(mappedBy = "tamanho")
		private List<Armazem> armazens;
		
		
		
		
		public List<Armazem> getClientes() {
			return armazens;
		}

		public void setArmazens(List<Armazem> armazens) {
			this.armazens = armazens;
		}

		public String getNome() {
			return nome;
		}

		public void setNome(String nome) {
			this.nome = nome;
		}
		
		public Integer getCapacidade() {
			return capacidade;
		}

		public void setCapacidade(Integer capacidade) {
			this.capacidade = capacidade;
		}
		
		
		
	
}
                                                                                                                                                                                                                 ProjectJava2023N/src/main/java/br/edu/toledoprudente/projectJava2023/pojo/Unidade.java              0000644 0001750 0000144 00000004534 14414646325 031451  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  package br.edu.toledoprudente.projectJava2023.pojo;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import jakarta.validation.constraints.*;

@Entity
@Table(name = "unidade")
public class Unidade extends AbstractEntity<Integer> {
	
	@NotBlank(message = "Informe o nome")
	@Size(min = 3, max = 150, message = "O nome deve conter entre 3 a 150 caracteres")
	@Column(length = 150, nullable = false)
	private String nome;

	@PositiveOrZero(message = "Informe uma quantidade total igual ou maior que 0")
	@Column(nullable = false)
	private Integer quantidade;
	
	@Column(name="valor", nullable = true, columnDefinition = "DECIMAL(10,2) DEFAULT 0.00")
	@NumberFormat(style = Style.CURRENCY, pattern = "#,##0.00")
	private BigDecimal valor;
	
	@Column(name="image", length = 300)
	private String image;
	
	//@DateTimeFormat(iso = ISO.DATE)
	//private LocalDate data;
	
	@NotNull(message = "Selecione o armazem")
	@ManyToOne
	@JoinColumn(name="armazem_id")
	private Armazem armazem;
	
	@NotNull(message = "Selecione o artefato")
	@ManyToOne
	@JoinColumn(name="artefato_id")
	private Artefato artefato;
	
	

	public Armazem getArmazem() {
		return armazem;
	}

	public void setArmazem(Armazem armazem) {
		this.armazem = armazem;
	}

	public Artefato getArtefato() {
		return artefato;
	}

	public void setArtefato(Artefato artefato) {
		this.artefato = artefato;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal rendimento) {
		this.valor = rendimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}
	
    public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	
	
	
}
                                                                                                                                                                    ProjectJava2023N/src/main/resources/                                                                0000755 0001750 0000144 00000000000 14410340616 020343  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/resources/static/                                                         0000755 0001750 0000144 00000000000 14410340616 021632  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/resources/templates/                                                      0000755 0001750 0000144 00000000000 14410623125 022340  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/resources/templates/fragmentos.html                                       0000644 0001750 0000144 00000002103 14406200414 025364  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  <html xmlns:th="http://www.thymeleaf.org">

<head>

</head>

<body>
	<div th:fragment="topo">
		<h1>Meu site</h1>
		<ul class="nav">
			<li class="nav-item">
				<a class="nav-link active" th:href="@{/categoria/listar}">Categoria</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" th:href="@{/cliente/listar}">Cliente</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Link</a>
			</li>
			<li class="nav-item">
				<a class="nav-link disabled" href="#">Desativado</a>
			</li>
		</ul>
	</div>

	<div th:fragment="rodape">
		&COPY; 2023
	</div>
	
	<div th:fragment="script">
		<script>
		 var url = "";
    $(".excluir").on("click", function(){    
// no clique do botão excluir, guardar a url para ser chamada na confirmação da janela modal
	url = $(this).attr("data-value");
    	url = url.replace("{id}", $(this).attr("id"));
     });
    
    $("#ok_confirm").click(function(){   // botão de confirmação da janela modal, chamará a controller
 	   document.location.href="http://localhost:8080" + url;
    });
		</script>
	</div>
	
	
</body>

</html>                                                                                                                                                                                                                                                                                                                                                                                                                                                             ProjectJava2023N/src/main/resources/templates/armazem/                                              0000755 0001750 0000144 00000000000 14410362770 024002  5                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  ProjectJava2023N/src/main/resources/templates/armazem/index.html                                    0000644 0001750 0000144 00000004643 14410422001 025766  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  <!DOCTYPE html>
<html lang="pt-br">

<head>
	<!-- Meta tags Obrigatórias -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
		integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<title>Armazém</title>
</head>

<body>
	<header th:include="fragmentos::topo">
	</header>
	<main class="container">
		<h1>Armazem</h1>

		<form method="post" th:action="@{/armazem/save}" 
							th:object="${armazem}">
			<div class="form-group">
				<label for="codigo">Código</label>
				<input th:field="*{id}" type="text" readonly class="form-control" id="codigo">

			</div>
			<div class="form-group">
				<label for="identificacao">Identificacao</label>
				<input th:field="*{identificacao}" type="text" class="form-control" id="identificacao"
					placeholder="identificacão...">
			</div>
			
			
			<div class="form-group">
				<label for="tamanho">Tamanho</label>
				<select class="form-control" th:field="*{tamanho}">
					<option value="">Selecione um tamanho</option>
					<option th:each="item:${list_tamanho}" 
					th:value="${item.id}"
					th:text="${item.nome+" "+item.capacidade}"
					 ></option>
				</select>
			</div>

			<button type="submit" class="btn btn-primary">Salvar</button>
		</form>

		<div th:if="${retorno} !=null" class="row">

			<span th:if="${retorno}" class="alert alert-success" th:text="${mensagem}"></span>
			<span th:if="!${retorno}" class="alert alert-danger" th:text="${mensagem}"></span>
		</div>


	</main>
	<footer th:include="fragmentos::rodape">
	</footer>
	<!-- JavaScript (Opcional) -->
	<!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
		integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
		crossorigin="anonymous"></script>
</body>

</html>                                                                                             ProjectJava2023N/src/main/resources/templates/armazem/list.html                                     0000644 0001750 0000144 00000006562 14410424315 025646  0                                                                                                    ustar   adrielbenvindo                  users                                                                                                                                                                                                                  <!DOCTYPE html>
<html lang="pt-br">

<head>
	<!-- Meta tags Obrigatórias -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
		integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

	<title>Armazéns</title>
</head>

<body>
	<header th:include="fragmentos::topo">
	</header>
	<main class="container">
		
		<h1>Armazéms</h1>
	
		<div style="margin:20px">
			<a class="btn btn-primary"
			 th:href="@{/armazem/new}">
			 	Incluir
			 </a>
		</div>

		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">Código</th>
					<th scope="col">Nome</th>
					<th scope="col">Tamanho</th>
					<th scope="col">Alterar</th>
					<th scope="col">Excluir</th>
				</tr>
			</thead>
			<tbody>
				<tr th:each="item:${list}">
					<th scope="row"
					<th:text="${item.id}"></th>
					<td th:text="${item.identificacao}"></td>
					<td th:text="${item.tamanho.nome}"></td>
					<td><a class="btn btn-info" th:href="@{/armazem/change(id=${item.id})}"> Alterar</a></td>	
					<td><button  class="btn btn-info excluir" data-target="#myModal" type="button" th:id="${item.id}" data-value="/armazem/delete?id={id}" data-toggle="modal" >Excluir</button></td>
				</tr>
			
			</tbody>
		</table>
		
		<!--CONFIRM MODAL-->
    <div class="modal fade" tabindex="-1" role="dialog" id="